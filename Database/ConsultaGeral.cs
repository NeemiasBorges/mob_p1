﻿using System;
using DataBase;
using Controllers;
using System.Data;
using System.Web.UI.WebControls;
using Utils;
using static PowerOne.Util.Log;
using PowerOne.Util;

namespace Models
{
    public class ConsultaGeral
    {
        private string sQuery = string.Empty;

        #region Verifica Tabela
        public bool VerificaTabela(string sTabela)
        {
            bool bExiste = false;
            DataTable table;

            try
            {
                if (ConstantUtils.Servidor != "Server2016\\SQLEXPRESS")
                {
                    sQuery = $"SELECT \"TableName\" FROM {ConstantUtils.SAP_BASE}.OUTB WHERE \"TableName\"='{sTabela}'";
                }
                else
                {
                    sQuery = $"SELECT \"TableName\" FROM OUTB WHERE \"TableName\"='{sTabela}'";
                }                

                //Realiza a consulta
                table = Conexao.ConsultaBanco(sQuery);

                if (table.Rows.Count > 0)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        bExiste = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Gravar($"Falha ao verificar tabela de usuário {sTabela} {ex.Message}", TipoLog.Erro);
            }

            return bExiste;
        }
        #endregion

        #region Verifica Campo
        public bool VerificaCampo(string sCampo, string sTabela)
        {
            bool bExiste = false;
            DataTable table;

            try
            {
                if (ConstantUtils.Servidor != "Server2016\\SQLEXPRESS")
                {
                    sQuery = "SELECT \"FieldID\" FROM " + ConstantUtils.SAP_BASE + ".CUFD WHERE \"TableID\"='" + sTabela + "' AND \"AliasID\"='" + sCampo + "'";
                }
                else
                {
                    sQuery = $"SELECT \"FieldID\" FROM CUFD WHERE \"TableID\"='{sTabela}' AND \"AliasID\"='{sCampo}'";
                }

                //Realiza a consulta

                table = Conexao.ConsultaBanco(sQuery);

                if (table.Rows.Count > 0)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        bExiste = true;

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Gravar("Falha ao verificar campo de usuário " + sCampo + " " + ex.Message, TipoLog.Erro);
            }

            table = null;

            return bExiste;
        }
        #endregion 
    }
}
