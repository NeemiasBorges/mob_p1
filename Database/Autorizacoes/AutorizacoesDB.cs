﻿using Database;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Database.Autorizacoes
{
    public class AutorizacoesDB
    {
        bool bCriou = false;

        ConsultaGeral oConsultaGeral = new ConsultaGeral();

        public void CreateTable()
        {
            if (!oConsultaGeral.VerificaTabela("P1_ACCESS".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_ACCESS", "P1 - Autorizações", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void CreateFields()
        {
            List<ValoresValidos> oValoresValidos = new List<ValoresValidos>();
            oValoresValidos.Add(new ValoresValidos { Valor = "N", Descricao = "Sem Autorização" });
            oValoresValidos.Add(new ValoresValidos { Valor = "R", Descricao = "Acesso Restrito" });
            oValoresValidos.Add(new ValoresValidos { Valor = "T", Descricao = "Acesso Total" });

            List<ValoresValidos> oValoresValidosYorN = new List<ValoresValidos>();
            oValoresValidosYorN.Add(new ValoresValidos { Valor = "Y", Descricao = "sim" });
            oValoresValidosYorN.Add(new ValoresValidos { Valor = "N", Descricao = "não" });

            if (!oConsultaGeral.VerificaCampo("P1_ACCESS", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Password", "@P1_ACCESS", "Password", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "NOVO", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_EMPId", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_EMPId", "@P1_Access", "EmpId", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Type", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Type", "@P1_Access", "Tipo de Usuário", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, "User", null);
            }

            #region Estoque
            if (!oConsultaGeral.VerificaCampo("P1_ReqS", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_ReqS", "@P1_Access", "Consulta Req Saida", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", oValoresValidos);
            }
            if (!oConsultaGeral.VerificaCampo("P1_NewReqS", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_NewReqS", "@P1_Access", "Nova Requisição", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", oValoresValidos);
            }
            if (!oConsultaGeral.VerificaCampo("P1_AUTORIZ", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_AUTORIZ", "@P1_Access", "Autorizador", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", oValoresValidos);
            }
            #endregion

            #region Almoxarifado
            if (!oConsultaGeral.VerificaCampo("P1_ALMOX", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_ALMOX", "@P1_Access", "Almoxarifado", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", oValoresValidos);
            }
            #endregion

            if (!oConsultaGeral.VerificaCampo("P1_Grupo", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Grupo", "@P1_Access", "Grupo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_MACaddress", "@P1_Access"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_MACaddress", "@P1_Access", "ID Dispositivo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 12, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_Status", "@P1_Access"))
            {
                List<ValoresValidos> oValoresValidosLogin = new List<ValoresValidos>();
                oValoresValidosLogin.Add(new ValoresValidos { Valor = "L", Descricao = "Logado" });
                oValoresValidosLogin.Add(new ValoresValidos { Valor = "D", Descricao = "Deslogado" });

                bCriou = MetaData.CriaCampoUsuario("P1_Status", "@P1_Access", "Status de Login", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "D", oValoresValidosLogin);
            }

            # region P1_AccGrp
            if (!oConsultaGeral.VerificaCampo("P1_AUTORIZ", "@P1_AccGrp"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_AUTORIZ", "@P1_AccGrp", "Autorizador", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", oValoresValidos);
            }
            if (!oConsultaGeral.VerificaCampo("P1_ALMOX", "@P1_AccGrp"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_ALMOX", "@P1_AccGrp", "Almoxarifado", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", oValoresValidos);
            }
            #endregion
        }
    }
}