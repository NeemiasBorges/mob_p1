﻿using Database.Estoque;
using PortalMOB.Database.Autorizacoes;
using PortalMOB.Database.Estoque;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database
{
    public static class CreateTablesAndFields
    {
        public static void Main()
        {
            Seeds();

            AutorizacoesDB();

            RequerimentoEstoque();

            cadastraUDFOitm();

            TabelasAux();
        }

        #region Seeds
        public static void Seeds()
        {
            //_autorizacoesRepository.CriarAutorizacoes();
        }
        #endregion

        #region Requerimento Estoque
        public static void RequerimentoEstoque()
        {
            RequerimentoEstoqueDB requerimentoEstoqueDB = new RequerimentoEstoqueDB();
            RequerimentoEstoqueLinhasDB requerimentoEstoqueLinhasDB = new RequerimentoEstoqueLinhasDB();

            requerimentoEstoqueDB.CreateTable();
            requerimentoEstoqueDB.AddField();

            requerimentoEstoqueLinhasDB.CreateTable();
            requerimentoEstoqueLinhasDB.AddField();
        }
        #endregion

        #region cadastro de item
        public static void cadastraUDFOitm()
        {
            Item OITMudfs = new Item();

            OITMudfs.CreateTable();
            OITMudfs.AddField();
        
        }
        #endregion

        #region Autorizações
        public static void AutorizacoesDB()
        {
            AutorizacoesDB autorizacoes = new AutorizacoesDB();

            autorizacoes.CreateTable();
            autorizacoes.CreateFields();
        }
        #endregion

        #region Add Tabelas Auxiliares e de log
        public static void TabelasAux()
        {
            Log log = new Log();
            log.CreateTable();
            log.AddField();
        }
        #endregion
    }
}