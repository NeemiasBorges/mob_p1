﻿//using PowerOne.Repositorio;
using Database;
using Models;
using PortalMOB.Database;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database.Estoque
{
    public class RequerimentoEstoqueLinhasDB
    {
        bool bCriou = false;

        ConsultaGeral oConsultaGeral = new ConsultaGeral();

        public void CreateTable()
        {
            if (!oConsultaGeral.VerificaTabela("P1_IGE1".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_IGE1", "P1 - Req. Saida LINHAS ", SAPbobsCOM.BoUTBTableType.bott_DocumentLines);
            }
        }
        public void AddField()
        {

            if (!oConsultaGeral.VerificaCampo("P1_ID", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_ID", "@P1_IGE1", "ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_LinStatus", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_LinStatus", "@P1_IGE1", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_xPed", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_xPed", "@P1_IGE1", "Pedido", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_ItemCode", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_ItemCode", "@P1_IGE1", "Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_PN", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_PN", "@P1_IGE1", "CODIGO PN", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_DTEFET", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_DTEFET", "@P1_IGE1", "DATA EFETIVAÇÃO", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Quantity", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Quantity", "@P1_IGE1", "Quantidade", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 15, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Comments", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Comments", "@P1_IGE1", "Observações", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_WhsCode", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_WhsCode", "@P1_IGE1", "Depósito", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null);
            }
            #region centros de custo

            if (!oConsultaGeral.VerificaCampo("P1_CC01", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_CC01", "@P1_IGE1", "Centro custo 01", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_CC02", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_CC02", "@P1_IGE1", "Centro custo 02", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_CC03", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_CC03", "@P1_IGE1", "Centro custo 03", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_CC04", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_CC04", "@P1_IGE1", "Centro custo 04", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_CC05", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_CC05", "@P1_IGE1", "Centro custo 05", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            } 
            

            if (!oConsultaGeral.VerificaCampo("P1_OcrName", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_OcrName", "@P1_IGE1", "Nome Centro custo 01", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 70, "", null);
            } 
            
            if (!oConsultaGeral.VerificaCampo("P1_OcrName2", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_OcrName2", "@P1_IGE1", "Nome Centro custo 02", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 70, "", null);
            } 
            
            if (!oConsultaGeral.VerificaCampo("P1_OcrName3", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_OcrName3", "@P1_IGE1", "Nome Centro custo 03", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 70, "", null);
            } 
            
            if (!oConsultaGeral.VerificaCampo("P1_OcrName4", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_OcrName4", "@P1_IGE1", "Nome Centro custo 04", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 70, "", null);
            } 
            
            if (!oConsultaGeral.VerificaCampo("P1_OcrName5", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_OcrName5", "@P1_IGE1", "Nome Centro custo 05", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 70, "", null);
            }

            #endregion

            if (!oConsultaGeral.VerificaCampo("P1_PROJ", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_PROJ", "@P1_IGE1", "Codigo do Projeto", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }
      

            if (!oConsultaGeral.VerificaCampo("P1_GroupItem", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_GroupItem", "@P1_IGE1", "Grupo do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_UndMedida", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_UndMedida", "@P1_IGE1", "Unidade de Medida", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_WhsName", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_WhsName", "@P1_IGE1", "Nome deposito", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 141, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_DescItem", "@P1_IGE1"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_DescItem", "@P1_IGE1", "Descrição do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, "", null);
            }


            if (!oConsultaGeral.VerificaCampo("P1_StatusLinha", "@P1_IGE1"))
            {
                List<ValoresValidos> statusLinha = new List<ValoresValidos>();
                statusLinha.Add(new ValoresValidos { Valor = "A", Descricao = "Aberta" });
                statusLinha.Add(new ValoresValidos { Valor = "C", Descricao = "Fechada" });

                bCriou = MetaData.CriaCampoUsuario("P1_StatusLinha", "@P1_IGE1", "Status da Linha", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "A", null);
            }
        }
    }
}