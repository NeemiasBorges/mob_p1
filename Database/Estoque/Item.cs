﻿using Models;
using PortalMOB.Database;
using System.Collections.Generic;

namespace Database.Estoque
{
    public class Item
    {
        bool bCriou = false;
        ConsultaGeral oConsultaGeral = new ConsultaGeral();

        public void CreateTable()
        {

        }
        public void AddField()
        {


            if (!oConsultaGeral.VerificaCampo("P1_ITMCT", "OITM"))
            {
                List<ValoresValidos> oValoresValidos = new List<ValoresValidos>();

                oValoresValidos.Add(new ValoresValidos { Valor = "U", Descricao = "Uso e Consumo" });
                oValoresValidos.Add(new ValoresValidos { Valor = "I", Descricao = "Instalavel" });
                oValoresValidos.Add(new ValoresValidos { Valor = "E", Descricao = "Equipamentos" });

                bCriou = MetaData.CriaCampoUsuario("P1_ITMCT", "OITM", "Categoria do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100 , "U", oValoresValidos);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Localizacao", "OITM"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Localizacao", "OITM", "Localização do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Entidade", "OITM"))
            {
                List<ValoresValidos> oEntidades = new List<ValoresValidos>();

                oEntidades.Add(new ValoresValidos { Valor = "C", Descricao = "CLIENTE" });
                oEntidades.Add(new ValoresValidos { Valor = "F", Descricao = "FORNECEDOR" });
                oEntidades.Add(new ValoresValidos { Valor = "T", Descricao = "TECNICO" });
                oEntidades.Add(new ValoresValidos { Valor = "O", Descricao = "OUTRO" });

                bCriou = MetaData.CriaCampoUsuario("P1_Entidade", "OITM", "Entidade do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "O", oEntidades);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Contrato", "OITM"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Contrato", "OITM", "Contrato do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_Protocolo", "OITM"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Protocolo", "OITM", "Protocolo do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "", null);
            }
            if (!oConsultaGeral.VerificaCampo("P1_NSerie", "OITM"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_NSerie", "OITM", "Numero de Serie do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "", null);
            }


        }

    }
}
