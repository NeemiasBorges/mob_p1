﻿using Database;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Database.Estoque
{
    public class Log
    {
        bool bCriou = false;

        ConsultaGeral oConsultaGeral = new ConsultaGeral();
        public void CreateTable()
        {
            if (!oConsultaGeral.VerificaTabela("P1_LOGMOB".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_LOGMOB", "P1 - Tbl Movimentos SME", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement);
            }

            if (!oConsultaGeral.VerificaTabela("P1_LOCALZ".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_LOCALZ", "P1 - Tbl Localizações", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement);
            }

            if (!oConsultaGeral.VerificaTabela("P1_DFEITS".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_DFEITS", "P1 - Tbl Defeitos", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement);
            }
            if (!oConsultaGeral.VerificaTabela("P1_TEMA".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_TEMA", "P1 - Tbl Temas", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }


        }
        public void AddField()
        {
            #region tabela temas
            if (!oConsultaGeral.VerificaCampo("P1_Color", "@P1_TEMA"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Color", "@P1_TEMA", "Cor do Menu", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_Icones", "@P1_TEMA"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Icones", "@P1_TEMA", "Tipo do Logo", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, null);
            }

            #endregion

            #region Tabela de Log

            List<ValoresValidos> status = new List<ValoresValidos>();
            status.Add(new ValoresValidos { Valor = "O", Descricao = "Aberto" });

            if (!oConsultaGeral.VerificaCampo("P1_DtLog", "@P1_LOGMOB"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_DtLog", "@P1_LOGMOB", "Data do Log", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11, null, null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_LocAntes", "@P1_LOGMOB"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_LocAntes", "@P1_LOGMOB", "Localização anterior", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null, null,  "P1_LOCALZ");
            }

            if (!oConsultaGeral.VerificaCampo("P1_LocAtual", "@P1_LOGMOB"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_LocAtual", "@P1_LOGMOB", "Localização atual", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null, null, "P1_LOCALZ");
            }

            if (!oConsultaGeral.VerificaCampo("P1_Aprovador", "@P1_LOGMOB"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Aprovador", "@P1_LOGMOB", "Aprovador da SME", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null, null);
            }
            
            if (!oConsultaGeral.VerificaCampo("P1_Item", "@P1_LOGMOB"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_Item", "@P1_LOGMOB", "Item da SME", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null, null);
            }

            #endregion

        }

    }
}