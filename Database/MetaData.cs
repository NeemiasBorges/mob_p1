﻿using DAL;
using DTO;
using IntegradorDaFonte.DTO.Conexao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Utils;
using PowerOne.Util;
using static PowerOne.Util.Log;
using PortalMOB.Database;

namespace Database
{
    static class MetaData
    {
        #region Cria Tabelas pela Service Layer
        public static bool CriaTabelasUsuario(string sNomeTabela, string sDescricaoTabela, SAPbobsCOM.BoUTBTableType oTipoTabela)
        {
            bool bCriou = false;

            try
            {
                CriaTabelaDTO criaTabelaDTO = new CriaTabelaDTO();

                criaTabelaDTO.TableName = sNomeTabela;
                criaTabelaDTO.TableDescription = sDescricaoTabela;
                criaTabelaDTO.TableType = oTipoTabela.ToString();

                var json = JsonConvert.SerializeObject(criaTabelaDTO);

                string retorno = RequestUtils.Request(ConstantUtils.URL_ADD_USERTABLES, RequestUtils.POST, LoginDAL.sessionId, json, false);

                if (!retorno.Contains("Erro:"))
                {
                    Log.Gravar("Tabela de usuario: " + sDescricaoTabela + " Criada com Sucesso", TipoLog.Erro);
                    bCriou = true;
                }
                else
                {
                    Log.Gravar("Falha ao criar o Tabela de usuario: " + sDescricaoTabela, TipoLog.Erro);
                }
            }
            catch (Exception ex)
            {
                Log.Gravar("Erro: " + ex.Message, TipoLog.Erro);
                GC.Collect();
            }

            return bCriou;
        }
        #endregion

        #region Cria Campos pela Service Layer
        public static bool CriaCampoUsuario(string sNomeCampo, string sTabelaCampo, string sDescricaoCampo, SAPbobsCOM.BoFieldTypes oTipoCampo, SAPbobsCOM.BoFldSubTypes oSubTipoCampo, int iTamanhoCampo, string sPadrao, List<ValoresValidos> lstValores)
        {
            bool bCriou = false;

            try
            {
                CriaCampoDTO.CriaCampo oCampoUsuarioDTO = new CriaCampoDTO.CriaCampo();

                oCampoUsuarioDTO.Description = sDescricaoCampo;
                oCampoUsuarioDTO.Name = sNomeCampo;
                oCampoUsuarioDTO.SubType = oSubTipoCampo.ToString();
                oCampoUsuarioDTO.TableName = sTabelaCampo;
                oCampoUsuarioDTO.EditSize = iTamanhoCampo;
                oCampoUsuarioDTO.Type = oTipoCampo.ToString();

                if (lstValores != null)
                {
                    CriaCampoDTO.ValidValuesMD oValidValuesMD;
                    List<CriaCampoDTO.ValidValuesMD> oListValidValuesMD = new List<CriaCampoDTO.ValidValuesMD>();

                    oValidValuesMD = null;

                    foreach (ValoresValidos oVV in lstValores)
                    {
                        oValidValuesMD = new CriaCampoDTO.ValidValuesMD();
                        oValidValuesMD.Value = oVV.Valor;
                        oValidValuesMD.Description = oVV.Descricao;
                        oListValidValuesMD.Add(oValidValuesMD);
                    }

                    oCampoUsuarioDTO.ValidValuesMD = oListValidValuesMD;
                }


                var json = JsonConvert.SerializeObject(oCampoUsuarioDTO);

                string retorno = RequestUtils.Request(ConstantUtils.URL_ADD_USERFIELDS, RequestUtils.POST, LoginDAL.sessionId, json, false);

                if (!retorno.Contains("Erro:"))
                {
                    Log.Gravar("Campo de usuario: " + sDescricaoCampo + " Criado com Sucesso", TipoLog.Erro);
                    bCriou = true;
                }
                else
                {
                    Log.Gravar("Falha ao criar o Campo de usuario: " + sDescricaoCampo, TipoLog.Erro);
                }
            }
            catch (Exception ex)
            {
                Log.Gravar("Erro: " + ex.Message, TipoLog.Erro);
                GC.Collect();
            }
            return bCriou;
        }
        #endregion
        
        #region Cria Campos pela Service Layer com Campo vinculado
        public static bool CriaCampoUsuario(string sNomeCampo, string sTabelaCampo, string sDescricaoCampo, SAPbobsCOM.BoFieldTypes oTipoCampo, SAPbobsCOM.BoFldSubTypes oSubTipoCampo, int iTamanhoCampo, string sPadrao, List<ValoresValidos> lstValores,string TblVinculada)
        {
            bool bCriou = false;

            try
            {
                CriaCampoDTO.CriaCampo oCampoUsuarioDTO = new CriaCampoDTO.CriaCampo();

                oCampoUsuarioDTO.Description = sDescricaoCampo;
                oCampoUsuarioDTO.Name = sNomeCampo;
                oCampoUsuarioDTO.SubType = oSubTipoCampo.ToString();
                oCampoUsuarioDTO.TableName = sTabelaCampo;
                oCampoUsuarioDTO.EditSize = iTamanhoCampo;
                oCampoUsuarioDTO.Type = oTipoCampo.ToString();

                if (!string.IsNullOrEmpty(TblVinculada))
                {
                    oCampoUsuarioDTO.LinkedTable = TblVinculada;
                }

                if (lstValores != null)
                {
                    CriaCampoDTO.ValidValuesMD oValidValuesMD;
                    List<CriaCampoDTO.ValidValuesMD> oListValidValuesMD = new List<CriaCampoDTO.ValidValuesMD>();

                    oValidValuesMD = null;

                    foreach (ValoresValidos oVV in lstValores)
                    {
                        oValidValuesMD = new CriaCampoDTO.ValidValuesMD();
                        oValidValuesMD.Value = oVV.Valor;
                        oValidValuesMD.Description = oVV.Descricao;
                        oListValidValuesMD.Add(oValidValuesMD);
                    }

                    oCampoUsuarioDTO.ValidValuesMD = oListValidValuesMD;
                }


                var json = JsonConvert.SerializeObject(oCampoUsuarioDTO);

                string retorno = RequestUtils.Request(ConstantUtils.URL_ADD_USERFIELDS, RequestUtils.POST, LoginDAL.sessionId, json, false);

                if (!retorno.Contains("Erro:"))
                {
                    Log.Gravar("Campo de usuario: " + sDescricaoCampo + " Criado com Sucesso", TipoLog.Erro);
                    bCriou = true;
                }
                else
                {
                    Log.Gravar("Falha ao criar o Campo de usuario: " + sDescricaoCampo, TipoLog.Erro);
                }
            }
            catch (Exception ex)
            {
                Log.Gravar("Erro: " + ex.Message, TipoLog.Erro);
                GC.Collect();
            }
            return bCriou;
        }
        #endregion
    }
}
