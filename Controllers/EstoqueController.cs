﻿using PortalMOB.Repositories.Interfaces;
using System.Web.Mvc;
using PowerOne.Controllers;
using PowerOne.Models;
using Models.Estoque;
using System.Collections.Generic;
using Models;
using PortalMOB.Models;
using Model.Estoque;
using System.Linq;
using System.Threading;
using System.Globalization;

namespace PortalMOB.Controllers
{
    [Authorize]
    public class EstoqueController : Controller
    {        
        #region Instancing Repositories 
        private readonly IEstoqueRepository _estoqueRepository;
        private readonly IConfiguracoesRepository _configuracoesRepository;
        private readonly IItemEstoqueRepository _itemEstoqueRepository;
        private readonly IFiliaisRepository _filiaisRepository;
        private readonly IDepositoRepository _depositoRepository;
        private readonly ICentroCursoRepository _centroCustoRepository;

        public EstoqueController(
            IEstoqueRepository estoqueRepository,
            IConfiguracoesRepository configuracoesRepository,
            IItemEstoqueRepository ItemEstoqueRepository,
            IFiliaisRepository filiaisRepository,
            IDepositoRepository depositoRepository,
            ICentroCursoRepository centroCustoRepository
            )
        {
            _depositoRepository = depositoRepository;
            _estoqueRepository = estoqueRepository;
            _configuracoesRepository = configuracoesRepository;
            _itemEstoqueRepository = ItemEstoqueRepository;
            _filiaisRepository = filiaisRepository;
            _centroCustoRepository = centroCustoRepository;
        }
        #endregion

        #region Index / List
        public ActionResult Index(string dtInicio, string dtFim, string status, string tipo)
        {
            Login login = (Login)Session["auth"];
            Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;

            if (autorizacoes.ReqEstoque == "N" && autorizacoes.Grupo.ReqEstoque == "N")
                return RedirectToAction("NaoAutorizado", "Home");

            List<RequerimentoEstoque> requerimentos = _estoqueRepository.List(
                login, dtInicio, dtFim, status, tipo
            );

            TempData["requerimentos"] = requerimentos;
            TempData["tipo"] = tipo;

            return View(requerimentos);
        }
        #endregion

        #region Details / GetByKey e ListarLinhas
        public ActionResult Details(int DocEntry, int Filial)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);

            Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;

            if (autorizacoes.ReqEstoque == "N" && autorizacoes.Grupo.ReqEstoque == "N")
                return RedirectToAction("NaoAutorizado", "Home");

            List<Deposito> deposito = _depositoRepository.List(Filial);
            TempData["depositos"] = deposito;

            List<ItemEstoque> itens = _itemEstoqueRepository.List();
            TempData["itens"] = itens;

            int[] dimensoesCtCusto = _configuracoesRepository.GetDimencoes();
            TempData["dimencoes"] = dimensoesCtCusto;

            if (dimensoesCtCusto.Contains(1))
                TempData["ctCusto"] = _centroCustoRepository.List(1);
            if (dimensoesCtCusto.Contains(2))
                TempData["ctCusto2"] = _centroCustoRepository.List(2);
            if (dimensoesCtCusto.Contains(3))
                TempData["ctCusto3"] = _centroCustoRepository.List(3);
            if (dimensoesCtCusto.Contains(4))
                TempData["ctCusto4"] = _centroCustoRepository.List(4);
            if (dimensoesCtCusto.Contains(5))
                TempData["ctCusto5"] = _centroCustoRepository.List(5);                                  

            RequerimentoEstoque solicitacao = _estoqueRepository.GetByKey(DocEntry);

            return View(solicitacao);
        }
        #endregion

        #region Edit/Add Line

        public ActionResult Edit(RequerimentoEstoqueLinhas linha, int tipo, int filial)
        {
            if (string.IsNullOrEmpty(linha.U_P1_ItemCode) || string.IsNullOrEmpty(linha.U_P1_Quantity) || string.IsNullOrEmpty(linha.U_P1_WhsCode) )
            {
                TempData["error"] = "Para editar o documento, todos os campos devem ser preenchidos";
            }
            else
            {
                linha.U_P1_Quantity = linha.U_P1_Quantity.Replace(",", ".");
                string result = null;
                result = _estoqueRepository.EditAdd(linha, tipo);

                if (result != "Atualizado com sucesso")
                {
                    TempData["success"] = result;
                } else
                {
                    TempData["error"] = result;
                }
            }


            return RedirectToAction("Details", new {DocEntry = linha.DocEntry, Filial = filial});
        }
        #endregion

        #region Create / Create
        public ActionResult Create()
        {
            Login login = (Login)Session["auth"];
            //Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;
            //if (
            //    autorizacoes.CriarReqEstoque == "N" &&
            //    autorizacoes.Grupo.CriarReqEstoque == "N"
            //   )
            //    return RedirectToAction("NaoAutorizado", "Home");

            ConfDocumento configs = _configuracoesRepository.GetConfigurationsOfDocuments();
            TempData["config"] = configs;

            List<ItemEstoque> itens = _itemEstoqueRepository.List();
            List<Filiais> filiais = _filiaisRepository.List();
            int[] dimensoesCtCusto = _configuracoesRepository.GetDimencoes();
            List<Projetos> projetos = _estoqueRepository.getProjeto();


            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoRepository.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoRepository.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoRepository.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoRepository.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoRepository.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }

            TempData["projetos"] = projetos;
            TempData["dimencoes"] = dimensoesCtCusto;
            TempData["ItensEstoque"] = itens;
            TempData["filiais"] = filiais;



            return View();
        }
        #endregion

        #region Add Estoque
        [HttpPost]
        public JsonResult Add(RequerimentoEstoque ReqEstoque)
        {
            var resultado = new { msg = "", type = "" };

            //int[] dimencoes = _configuracoesRepository.GetDimencoes();

            Resultado resul = _estoqueRepository.AddReq(ReqEstoque,(Login)Session["auth"]);

            if (resul.Msg != "error")
            {
                resultado = new
                {
                    msg = "Solicitação de Material e Equipamento Adicionada com Sucesso",
                    type = "success"
                };
            }
            else
            {
                resultado = new
                {
                    msg = resul.Msg,
                    type = "error"
                };
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}