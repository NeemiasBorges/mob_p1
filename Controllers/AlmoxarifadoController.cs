﻿using Models;
using Models.Estoque;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity.Injection;

namespace PortalMOB.Controllers
{
    [Authorize]
    public class AlmoxarifadoController : Controller
    {
        private readonly IAlmoxarifadoRepository _almoxarifadoRepository;
        private readonly IEstoqueRepository _estoqueRepository;
        private readonly IDepositoRepository _depositoRepository;
        private readonly IConfiguracoesRepository _configuracoesRepository;
        private readonly ICentroCursoRepository _centroCustoRepository;
        public AlmoxarifadoController(
            IAlmoxarifadoRepository almoxarifadoRepository,
            IEstoqueRepository estoqueRepository,
            IDepositoRepository depositoRepository,
            ICentroCursoRepository centroCursoRepository,
            IConfiguracoesRepository configuracoesRepository
            )
        {
            _centroCustoRepository = centroCursoRepository;
            _configuracoesRepository = configuracoesRepository;
            _depositoRepository = depositoRepository;
            _estoqueRepository = estoqueRepository;
            _almoxarifadoRepository = almoxarifadoRepository;
        }

        #region Index / List
        public ActionResult Index(string dtInicio, string dtFim, string status, int tipo)
        {
            //Aprovado - tipo = 3
            //Em Curso - tipo = 4
            //Concluidos - tipo = 5
            Login login = (Login)Session["auth"];
            Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;

            if (
                autorizacoes.ReqEstoque == "N" &&
                autorizacoes.Grupo.ReqEstoque == "N"
               )
                return RedirectToAction("NaoAutorizado", "Home");

            List<RequerimentoEstoque> requerimentos = _almoxarifadoRepository.List(
                login, dtInicio, dtFim, status, tipo
            );
            TempData["tipo"] = tipo.ToString();
            TempData["requerimentos"] = requerimentos;
            return View();

        }
        #endregion

        #region details

        public ActionResult Details(int DocEntry)
        {
            Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;
            List<Deposito> deposito = new List<Deposito>();
            List<ItemEstoque> itens = new List<ItemEstoque>();
            if (
                         autorizacoes.ReqEstoque == "N" &&
                         autorizacoes.Grupo.ReqEstoque == "N"
                        )
                return RedirectToAction("NaoAutorizado", "Home");

            int[] dimensoesCtCusto = _configuracoesRepository.GetDimencoes();
            if (dimensoesCtCusto.Contains(1))
            {
                List<CentroCusto> centroCustos = _centroCustoRepository.List(1);
                TempData["ctCusto"] = centroCustos;
            }

            if (dimensoesCtCusto.Contains(2))
            {
                List<CentroCusto> centroCustos2 = _centroCustoRepository.List(2);
                TempData["ctCusto2"] = centroCustos2;
            }
            if (dimensoesCtCusto.Contains(3))
            {
                List<CentroCusto> centroCustos3 = _centroCustoRepository.List(3);
                TempData["ctCusto3"] = centroCustos3;
            }
            if (dimensoesCtCusto.Contains(4))
            {
                List<CentroCusto> centroCustos4 = _centroCustoRepository.List(4);
                TempData["ctCusto4"] = centroCustos4;
            }
            if (dimensoesCtCusto.Contains(5))
            {
                List<CentroCusto> centroCustos5 = _centroCustoRepository.List(5);
                TempData["ctCusto5"] = centroCustos5;
            }
            TempData["dimencoes"] = dimensoesCtCusto;



            TempData["dimencoes"] = dimensoesCtCusto;

            TempData["depositos"] = deposito;
            TempData["itens"] = itens;

            RequerimentoEstoque requerimento = _estoqueRepository.GetByKey(DocEntry);

            return View(requerimento);

        }

        #endregion

        #region Em curso / Entregue

        public ActionResult AtualizaReqAlmoxarifado(int DocEntry, string Tipo, string status)
        {
            //Aprovado - tipo = 3
            //Em Curso - tipo = 4
            //Concluidos - tipo = 5

            //O - Aberto
            //A - Aprovada
            //P - Em Curso
            //R - Aguardando Retirada
            //Y - Concluido
            //C - Cancelada
            RequerimentoEstoque requerimento = _estoqueRepository.GetByKey(DocEntry);
            string result = "";
            if (status == "C")
            {
                result = _almoxarifadoRepository.AttReq(requerimento.DocEntry, status);
            }
            else
            {
                if (Tipo == "5")
                {
                    result = _almoxarifadoRepository.FecharRequerimento(requerimento, (Login)Session["auth"], status);
                }
                else
                {
                    result = _almoxarifadoRepository.AttReq(requerimento.DocEntry, status);
                }
            }

            if (result != "Atualizado com sucesso!" && result != "Concluido com sucesso")
            {
                TempData["error"] = result;
            }
            else
            {
                TempData["success"] = $"Documento {DocEntry} ";
                TempData["success"] += result;
            }

            return RedirectToAction(nameof(Index), new { tipo = Tipo });
        }

        #endregion
    }
}