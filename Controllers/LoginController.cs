﻿using System;
using System.Data;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.Security;
using Antlr.Runtime.Misc;
using Microsoft.Ajax.Utilities;
using PowerOne.Models;
using PowerOne.Repository;
using PowerOne.Util;
using System.Timers;
using PowerOne.Models.ViewModel;
using PortalMOB.Repositories.Interfaces;
using DAL;
using Utils;

namespace PowerOne.Controllers
{
    public class LoginController : Controller
    {

        #region Intancing Repositories 
        private readonly ILoginRepository _loginRepository;
        private readonly IConfiguracoesRepository _configuracoesRepository;
        private readonly IAutorizacoesRepository _autorizacoesRepository;

        public LoginController(
            ILoginRepository loginRepository,
            IAutorizacoesRepository autorizacoesRepository,
            IConfiguracoesRepository configuracoesRepository)
        {
            _loginRepository = loginRepository;
            _autorizacoesRepository = autorizacoesRepository;
            _configuracoesRepository = configuracoesRepository;
        }
        #endregion         

        #region GET Login 
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region POST Login
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Index(Login login)
        {
            if (!ModelState.IsValid)
                return View(login);

            string validado = isValid(login);

            if (validado == "Valid")
            {
                FormsAuthentication.SetAuthCookie(login.email, false);

                if (login.Password.ToUpper() == "NOVO")
                    return RedirectToAction(nameof(AlterarSenha));

                return RedirectToAction("Index", "Home");
            }   
            else
            {
                ModelState.AddModelError("email", "Email e/ou Senha incorreto(s)!");
                return View();
            }
        }
        #endregion

        #region Validação
        private string isValid(Login login)
        {
            string valid = null;

            #region consulta se cadastro do funcionario já existe 

            int id_User = 0;

            id_User = _loginRepository.ConsultaId(login); /*consulta o Id da OHEM*/
            
            if (id_User != 0)
            {
                bool temCadastro = _loginRepository.ConsultaCadastro(id_User); /*consulta na P1_ACESS o id*/
                
                if (!temCadastro)
                {
                    string msg = _loginRepository.AddAcess(login, id_User); /*add caso nao exista*/
                }
            }
            #endregion

            Login user = _loginRepository.Login(login);

            if (user != null)
            {                
                Tema tema = _configuracoesRepository.GetTema(user.UserId.ToString());
               
                valid = "Valid";
                if (tema != null)
                {
                    user.Tema = tema;
                }
                else
                {
                    _configuracoesRepository.ConfigurarTema(user.UserId.ToString(), "RED", "PRO");
                    user.Tema = _configuracoesRepository.GetTema(user.UserId.ToString());
                }

                _autorizacoesRepository.CriarAutorizacoes(user);

                user.Autorizacoes = _autorizacoesRepository.PegarAutorizacoes(user.UserId.ToString());
                user.Autorizacoes.Grupo = _autorizacoesRepository.PegarGrupoAutorizacoes(user.Autorizacoes.IdGrupo);
                user.MACaddress = _loginRepository.GetMacAddress().ToString();
                user.sessionId = LoginDAL.sessionId;
                
                Session["auth"] = user;
                //ConstantUtils.SESSION = user;
                //ConstantUtils.AUTHORIZATIONS = user.Autorizacoes;
            }

            return valid;
        }
        #endregion

        #region Alteração de Senha
        [Authorize]
        public ActionResult AlterarSenha()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarSenha(AlterarSenhaViewModel alterarSenha)
        {
            Login user;

            if (!ModelState.IsValid)
            {
                return View(alterarSenha);
            }

            Login paramsLogin = new Login() { email = alterarSenha.email, User = alterarSenha.CodPNLink, Password = alterarSenha.CurrentPassword };
            user = _loginRepository.Login(paramsLogin);
      
            if (user != null)
            {
                _loginRepository.EditarSenha(alterarSenha);

                Session["auth"] = user;
            }
            else
            {
                ModelState.AddModelError("CurrentPassword", "Senha atual incorreta!");
                return View(alterarSenha);
            }

            TempData["Success"] = "Senha alterada com sucesso!";
            return RedirectToAction("Logout");
        }
        #endregion

        #region Recuperar Senha
        [Authorize]
        public ActionResult RecuperarSenha()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecuperarSenha(RecuperarSenhaViewModel recuperarSenha)
        {
            if (!ModelState.IsValid)
            {
                return View(recuperarSenha);
            }

            ConfiguracoesEmail configEmail = _configuracoesRepository.GetConfiguracoesEmail();
            string IdUser = _loginRepository.GetIdUsuarioPorEmail(recuperarSenha.Email);
            if (String.IsNullOrEmpty(IdUser))
            {
                ModelState.AddModelError("Email", "Os dados inseridos não correspondem a um usuário cadastrado, VerIfique seu cadastro no SAP!");
                return View(recuperarSenha);
            }
            else
            {
                //editar senha
                AlterarSenhaViewModel paramsPassword = new AlterarSenhaViewModel()
                {
                    Id = int.Parse(IdUser),
                    //gerando senha com 10 caracteres
                    NewPassword = GeradorDeSenha.GetSenha(10)
                };

                _loginRepository.EditarSenha(paramsPassword);
                //_email.RecoverPasswordEmail(paramsPassword.NewPassword, recuperarSenha.Email, configEmail);

                TempData["Success"] = "Você recebeu um email com sua nova senha!";
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Logout
        [Authorize]
        public ActionResult Logout()
        {
            try
            {
                FormsAuthentication.SignOut();
                return Redirect(nameof(Index));
            }
            catch
            {
                return Redirect(nameof(Index));
            }            
        }
        #endregion  
    }
}