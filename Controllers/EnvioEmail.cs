﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Repository;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    [Authorize]
    public class EnvioEmail
    {

        #region Serviços
        ConfiguracoesRepository _configuracoesEmail = new ConfiguracoesRepository();
        #endregion


        #region SendMail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void EnviaEmail(string EmailEnvio,int tipoEnvio,string Solicitante,string Idoc,List<string> itens)
        {
            ConfiguracoesEmail configuracoesEmail = _configuracoesEmail.GetConfiguracoesEmail();

            MailAddress sDe = new MailAddress(configuracoesEmail.Email);
            MailMessage objEmail = new MailMessage();

            //'Define o Campo From e ReplyTo do e-mail.
            //objEmail.From = new System.Net.Mail.MailAddress(Conexao.Company.CompanyName + "< " + sDe + " >");
            objEmail.From = new System.Net.Mail.MailAddress($"< {sDe} >");

            objEmail.To.Add("<" + EmailEnvio + ">");

            objEmail.Priority = System.Net.Mail.MailPriority.Normal;
            objEmail.IsBodyHtml = true;
            objEmail.Subject = "Portal MOB: SME ";

            //Attachment logo = new Attachment(HostingEnvironment.MapPath(Constantes.DiretorioLogoLayoutEmail));

            //objEmail.Attachments.Add(logo);


            string prefixo = "<!DOCTYPE html>" +
                "<html>" +
                "<head>" +
                    "<title></title> " +
                "</head>" +
                "<body>" +
                "<div style=\"bgcolor:#F1F1F1;background-color:#F1F1F1; width: 100%; min-width:620px; table-layout:fixed; border-collapse:collapse; border-spacing:0px;\"> " +
                    "<table style=\"width: 60%; margin: auto;background-color:#ffffff; bgcolor:#ffffff\" border=\"0\"> " +
                        "<tr> " +
                            "<td align=\"center\" style=\"text-align:center\">" +
                                "<img src=\"https://i.imgur.com/3uO7w0u.png\"  alt=\"logo da empresa\" border=\"0\">" +
                            "</td>" +
                        "</tr>" +
                     "</tbody>" +
                 "</table>" +
            "<table style =\"width: 60%;background-color:#ffffff; margin: auto;font-family:Helvetica,sans-serif;color:#3D3D3D;font-size:14px;line-height:18px;bgcolor:#ffffff\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\"> " +
                 "<tr> " +
                     "<td align=\"center\">";

            string sufixo = " </td></tr></table></div></body></html>";

         
                string ListaItens = "<table style =\"width: 60%;border-collapse:collapse;font-weight:bold;text-align:center;margin: auto;font-family:Helvetica,sans-serif;color:#3D3D3D;font-size:14px;line-height:18px;\">" +
                                  "<thead>" +
                                      "<tr style=\"border-bottom: 1px solid #333333;text-align:center;\">" +
                                          "<th>Quantidade</th>" +
                                          "<th>Descrição</th>" +
                                      "</tr>" +
                                  "</thead>" +
                                  "<tbody>";
            foreach (var item in itens)
            {
                ListaItens += item;
            }
            ListaItens += "</tbody></table>";
            if (tipoEnvio == 1)
            {
                objEmail.Body = "" + 
                    prefixo +
                    Solicitante + ", Uma solicitação de Material foi feita, esse documento precisa de sua aprovação para seguir ao almoxarifado" +
                    ListaItens + sufixo;
            }
            else if (tipoEnvio == 2)
            {
                objEmail.Body = prefixo + "<p style:\"font-size:14px;line-height:18px;\";>" + Solicitante.Replace("."," ") + " sua Solicitação de Material foi enviada ao almoxarifado:</p>"
                    + ListaItens +
                    " <p style:\"font-size:14px;line-height:18px;\";>Aguarde até que a solicitação seja processada, você tambem consegue acompanhar a evolução no portal.</p>" + sufixo;
            }
            else if (tipoEnvio == 3)
            {
                objEmail.Body = "" +
                    prefixo +
                    Solicitante + ", Uma solicitação de Material foi feita, esse documento precisa de sua aprovação para finalizar" +
                    ListaItens + sufixo;
            }
            
            //else if (tipoDoc == "Reembolso")
            //{
            //    objEmail.Body = prefixo + "O usuario " + Solicitante + ", Acaba de adicionar um Reembolso, é possivel visualizar esse documento detalhamente em nosso portal " + sufixo;
            //}

            objEmail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            objEmail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
            System.Net.Mail.SmtpClient objSmtp = new System.Net.Mail.SmtpClient();

            objSmtp.Host = configuracoesEmail.Servidor;
            objSmtp.Port = int.Parse(configuracoesEmail.Porta);

            objSmtp.EnableSsl = true;
            objSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            objSmtp.UseDefaultCredentials = false;
            objSmtp.Credentials = new System.Net.NetworkCredential(configuracoesEmail.Email, configuracoesEmail.Senha);

            //try
            //{
            //    if (configuracoesEmail.Servidor == "smtp.office365.com")
            //    {
            //        objSmtp.TargetName = "STARTTLS/smtp.office365.com";
            //    }
            //    objSmtp.Send(objEmail);

            //    sEnviou = true;
            //}
            //catch (Exception ex)
            //{
            //    Log.Gravar(ex.Message, Log.TipoLog.Erro);
            //    sEnviou = false;
            //}
            //finally
            //{
                //excluímos o objeto de e-mail da memória
                objEmail.Dispose();
            //}

        }
        #endregion
    }
}