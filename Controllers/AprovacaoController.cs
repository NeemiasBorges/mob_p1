﻿using Models.Estoque;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Models;
using PowerOne.Repository;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PortalMOB.Controllers
{
    [Authorize]
    public class AprovacaoController : Controller
    {
        #region Dependencias
        private readonly IAprovacaoRepository _aprovacaoRepository;
        public AprovacaoController(
            IAprovacaoRepository aprovacaoRepository
        )
        {
            _aprovacaoRepository = aprovacaoRepository;
        }

        #endregion

        #region Index
        public ActionResult Index(string DataInicio, string DataFim, string Status)
        {
            TempData["requerimentos"] = _aprovacaoRepository.searchList(DataInicio, DataFim, Status);
            
            return View();
        }
        #endregion

        #region Aprovar
        public ActionResult Aprovar(string DocEntry)
        {
            string resul = null;

            resul = _aprovacaoRepository.aprovaReq(DocEntry, (Login)Session["auth"]);

            if (resul != "Atualizado com sucesso!")
            {
                TempData["error"] = resul;
            }
            else
            {
                TempData["success"] = resul;
            }

            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Fechar

        public ActionResult Fechar(string DocEntry)
        {

            string result = null;
            result = _aprovacaoRepository.fecharReq(DocEntry);
            return RedirectToAction(nameof(Index));

        }

        #endregion
    }
}