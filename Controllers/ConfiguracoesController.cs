﻿using PowerOne.Models;
using PowerOne.Repository;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using PortalMOB.Repositories.Interfaces;
using PortalMOB.Models;
using PortalMOB.Repositories;

namespace PowerOne.Controllers
{
    [Authorize]
    public class ConfiguracoesController : Controller
    {
        #region Intancing Repositories 
        private readonly IConfiguracoesRepository _configuracoesRepository;
        private readonly IAutorizacoesRepository _autorizacoesRepository;
        private readonly IFiliaisRepository _filiaisRepository;
        private readonly IDepositoRepository _depositoRepository;

        public ConfiguracoesController(
            IConfiguracoesRepository configuracoesRepository,
            IAutorizacoesRepository autorizacoesRepository,
            IFiliaisRepository filiaisRepository,
            IDepositoRepository depositoRepository
        )
        {
            _configuracoesRepository = configuracoesRepository;
            _autorizacoesRepository = autorizacoesRepository;
            _filiaisRepository = filiaisRepository;
            _depositoRepository = depositoRepository;
        }
        #endregion

        #region Configurar Email
        public ActionResult Email()
        {
            //if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
            //{
            //    return RedirectToAction("NaoAutorizado", "Home");
            //}

            ConfiguracoesEmail configuracoesEmail = _configuracoesRepository.GetConfiguracoesEmail();

            return View(configuracoesEmail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Email(ConfiguracoesEmail configEmail)
        {
            if (!ModelState.IsValid)
            {
                return View(configEmail);
            }

            string msg = _configuracoesRepository.configuraremail(configEmail);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = "Email Configurado com Sucesso!";
            }

            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Anexo(ConfiguracoesEmail confAnexo)
        //{
        //    string msg = _configuracoesService.ConfiguraAnexo(confAnexo);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Anexos do Email Configurados com Sucesso!";
        //    }

        //    return RedirectToAction(nameof(Email));
        //}
        #endregion

        #region Temas
        public ActionResult Temas()
        {
            return View();
        }

        #endregion

        #region Autorizações
        public ActionResult Autorizacoes()
        {
            Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;

            //if (autorizacoes.Tipo != "Adm")
            //    return RedirectToAction("NaoAutorizado", "Home");

            TempData["autorizacoes"] = _autorizacoesRepository.List();
            TempData["autorizacoesGrupo"] = _autorizacoesRepository.ListGrupo();

            return View();
        }

        [HttpPost]
        public ActionResult Autorizacoes(Autorizacoes autorizacoes)
        {
            Login login = (Login)Session["auth"];

            string msg = null;

            msg = _autorizacoesRepository.Update(autorizacoes);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = $"Autorizações atualizadas com sucesso.";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesRepository.PegarAutorizacoes(login.UserId.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesRepository.PegarGrupoAutorizacoes(login.Autorizacoes.IdGrupo);
            }

            return RedirectToAction(nameof(Autorizacoes));
        }
        #endregion

        #region Configurações de Documentos
        public ActionResult DocConfig()
        {
            TempData["Dimensoes"] = _configuracoesRepository.GetDimencoes();
            TempData["filiais"] = _filiaisRepository.List();
            TempData["Configs"] = _configuracoesRepository.GetConfigurationsOfDocuments();

            return View();
        }
        #endregion





        #region Trocar Icones
        [HttpPost]
        public ActionResult ChangeIcons(string icon)
        {
            Login login = (Login)Session["auth"];

            if (icon != null)
            {
                Resultado response = _configuracoesRepository.ConfigurarTema(login.Id.ToString(), null, icon);

                if (response.Msg != null)
                {
                    TempData["Error"] = response.Msg;
                }
                else
                {
                    ((Login)Session["auth"]).Tema = _configuracoesRepository.GetTema(login.Id.ToString());
                    TempData["Success"] = "icones alterados com sucesso!";
                }
            }
            else
            {
                TempData["Error"] = "Selecione um icone!";
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Upload de Logo
        //[HttpPost]
        //public ActionResult UploadLogo(HttpPostedFileBase logo)
        //{
        //    string msg = null;
        //    try
        //    {
        //        if (logo != null)
        //        {
        //            string path = Path.Combine(Server.MapPath("~/Content/img"), "logo-company.png");
        //            logo.SaveAs(path);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Logo do site alterada com sucesso!";
        //    }

        //    return RedirectToAction(nameof(Temas));
        //}
        #endregion

        #region Configurar Cor do Menu
        [HttpPost]
        public ActionResult TrocarCorMenu(string color)
        {
            Resultado resultado = new Resultado();
            if (color != null)
            {
                resultado = _configuracoesRepository.ConfigurarTema(((Models.Login)Session["auth"]).UserId.ToString(), color, null);

                if (resultado.Msg != null)
                {
                    TempData["Error"] = resultado.Msg;
                    Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                }
                else
                {
                    ((Models.Login)Session["auth"]).Tema = _configuracoesRepository.GetTema(((Models.Login)Session["auth"]).UserId.ToString());
                    TempData["Success"] = "Cor do menu alterado com sucesso!";
                }
            }
            else
            {
                TempData["Error"] = "Selecione uma cor!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion





        #region Autorizacoes para Grupo
        public ActionResult AtualizarAutorizacoesGrupo(AutorizacoesGrupo autorizacoes)
        {
            string msg = _autorizacoesRepository.AtualizarAutorizacoesGrupos(autorizacoes);

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Autorização atualizada com sucesso!";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesRepository.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesRepository.PegarGrupoAutorizacoes(((Login)Session["auth"]).Id.ToString());
            }

            return RedirectToAction("Autorizacoes");
        }
        #endregion

        #region Adicionar Grupo
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult AddGroup(string GroupName)
        //{
        //    string msg = null;

        //    msg = _autorizacoesService.CriarGrupos(GroupName, GroupName);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Grupo Criado com sucesso.";
        //        ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
        //        ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
        //    }

        //    return RedirectToAction(nameof(Autorizacoes));
        //}
        #endregion

        #region Adicionar/Atualizar o grupo de um usuário
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AtualizarGrupoUsuario(string idUser, string idGrupo)
        {
            string msg = null;
            if (String.IsNullOrEmpty(idGrupo))
            {
                msg = "Selecione um grupo.";
            }
            else
            {
                msg = _autorizacoesRepository.AtualizarGrupoDoUsuario(idUser, idGrupo);
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Grupo de usuário adicionado com sucesso.";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesRepository.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesRepository.PegarGrupoAutorizacoes(((Login)Session["auth"]).Id.ToString());
            }


            return RedirectToAction(nameof(Autorizacoes));
        }

        #endregion





        #region Configura Centros de Custo
        [HttpPost]
        public ActionResult CtCustoConfig(int[] ctCusto)
        {
            string msg = _configuracoesRepository.ConfigurarDimencoes(ctCusto);

            if (msg != "Atualizado com sucesso!")
                TempData["Error"] = msg;
            else
                TempData["Success"] = "Configurações de dimensões de centro de custo alterada com sucesso!";

            return RedirectToAction(nameof(DocConfig));
        }
        #endregion

        #region Filial e Depósito padrão
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DFPadrao(string idFilial, string idDeposito, bool ativado)
        {
            string msg = null;

            msg = _configuracoesRepository.ConfDFPadrao(idFilial, idDeposito, ativado);

            if (ativado)
            {
                TempData["Success"] = "Retirada dos valores padrões de Depósito e Filial feita com sucesso!";
                return RedirectToAction(nameof(DocConfig));
            }

            if (msg == null)
            {
                TempData["Success"] = "Depósito e Filial Padrões definidos com sucesso!";
            }
            else
            {
                TempData["Error"] = msg;
            }

            return RedirectToAction(nameof(DocConfig));
        }
        #endregion
    }
}