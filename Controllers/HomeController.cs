﻿using PortalMOB.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{

    public class HomeController : Controller
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AppendHeader("Refresh",String.Concat((Session.Timeout * 1),";URL=~/Login/Index.cshtml"));
        }
        #region Intancing Repositories  
        private readonly IEstoqueRepository _estoqueRepository;
        public HomeController(
        IEstoqueRepository estoqueRepository)
        {
            _estoqueRepository = estoqueRepository;
        }
        #endregion


        #region Index / Home
        public ActionResult Index()
        {            
            if (((PowerOne.Models.Login)Session["auth"]) == null)
                return RedirectToAction("Index", "Login");

            TempData["topItemMesAtual"] = _estoqueRepository.GetTopRequestedItemInMonth(DateTime.Now);
            TempData["topItemMesAnterior"] = _estoqueRepository.GetTopRequestedItemInMonth(DateTime.Now.AddMonths(-1));

            return View();
        }
        #endregion



        #region Estoque
        public ActionResult Estoque()
        {
            TempData["estoque"] = _estoqueRepository.contEstoque();
            return View();
        }
        #endregion

        #region Almoxarifado
        public ActionResult Almoxarifado()
        {
           // TempData["estoque"] = _estoqueRepository.contEstoque();
            return View();
        }
        #endregion


        #region Configurações
        public ActionResult Configuracoes()
        {
            return View();
        }
        #endregion


    }
}