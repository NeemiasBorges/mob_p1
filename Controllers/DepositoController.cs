﻿using Models;
using PortalMOB.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PortalMOB.Repositories.Interfaces;

namespace PortalMOB.Controllers
{
    [Authorize]
    public class DepositoController : Controller
    {
        #region Serviços (Consultas ao Banco de Dados)

        private readonly IDepositoRepository _depositoRepository;

        public DepositoController(IDepositoRepository estoqueRepository)
        {
            _depositoRepository = estoqueRepository;
        }
        #endregion

        #region Index
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Listar Depósitos
        public JsonResult ListDepositos(int Filial)
        {
            List<Deposito> depositos = _depositoRepository.List(Filial);

            return Json(depositos, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Search
        public ActionResult Search(int Filial, string deposito)
        {
            List<Deposito> depositos = _depositoRepository.Search(Filial, deposito);

            return Json(depositos, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}