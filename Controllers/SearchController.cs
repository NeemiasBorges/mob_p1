﻿using Models;
using PortalMOB.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalMOB.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        #region Intancing Repositories 

        private readonly IItemEstoqueRepository _itemEstoqueRepository;
        public SearchController(
             IItemEstoqueRepository ItemEstoqueRepository
        )
        {
            _itemEstoqueRepository = ItemEstoqueRepository;
        }

        #endregion


        // GET: Search
        #region search itens
        public ActionResult SearchItem(string name)
        {
            List<ItemEstoque> etapas = _itemEstoqueRepository.Search(name);

            return Json(etapas, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}