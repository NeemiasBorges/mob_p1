﻿using System.Web.Script.Serialization;
using Utils;
using Models;

namespace DAL
{
    public class UtilsClassDAL
    {
        protected JsonUtils jsUtils = null;
        protected MergeUtils mergeUtils = null;
        public JavaScriptSerializer js = null;
        protected JsonModel jsModel = null;
        protected BatchUtils batchUtils = null;
    }
}