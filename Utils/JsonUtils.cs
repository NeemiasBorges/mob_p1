﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
namespace Utils
{
    public class JsonUtils
    {
        #region MONTA STRING JSON DE RETORNO
        public string JsonRetorno(string status, string message, dynamic data, string metodo)
        {
            Regex regex = new Regex(@"\(.*?\)");
            MatchCollection matches = regex.Matches(message);
            string retorno;
            if (matches.Count > 0)
                retorno = "{\"status\" :\"" + matches[0].ToString().Replace("(", "").Replace(")", "") + "\", \"message\" :\"" + message.Replace("\"", "'") + "\", \"data\" :\"" + data + "\"}";
            else
                retorno = "{\"status\" :\"" + status + "\", \"message\" :\"" + message.Replace("\"", "'") + "\", \"data\" :\"" + data + "\"}";

            return retorno;
        }
        #endregion

        #region SALVA JSON EM TXT NO DIRETORIO DO PROJETO
        public bool SalvarJson(string json, string modulo, string identificador, string acao)
        {
            /* VERIFICA SE EXISTE DIRETORIO */
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory.ToString() + "/json/"))
                Directory.CreateDirectory((AppDomain.CurrentDomain.BaseDirectory.ToString() + "/json/"));

            /* CRIA O ARQUIVO ESPECIFICO  */
            string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "/json/" + modulo + "_" + identificador + "_" + acao + "_" + DateTime.Now.ToString("yyyy-MM-dd-HHmmssFFF") + ".txt";

            /* SALVA O CONTEUDO DA STRING NO ARQUIVO ESPECIFICO */
            try
            {
                File.WriteAllText(path, json);
                return true;
            }
            catch (Exception ex)
            {
                string nomeArquivo = AppDomain.CurrentDomain.BaseDirectory.ToString() + "log" + DateTime.Now.ToString("yyyy-MM-dd-HHmmssFFF") + ".txt";

                /* CRIA UM NOVO ARQUIVO E DEVOLVE UM STREAMWRITER PARA ELE */
                StreamWriter writer = new StreamWriter(nomeArquivo);
                writer.WriteLine(ex.Message);
                writer.Close();
                return false;
            }
        }
        #endregion

        #region TRATATIVA PARA REMOVER VALORES NULL DO JSON | EX: "campoChave" : []
        public string RemoveArrayVazioJson(string json)
        {
            var parsed = (JContainer)JsonConvert.DeserializeObject(json);
            var nodesToDelete = new List<JToken>();

            do
            {
                nodesToDelete.Clear();

                ClearEmpty(parsed, nodesToDelete);

                foreach (var token in nodesToDelete)
                {
                    token.Remove();
                }
            } while (nodesToDelete.Count > 0);

            return parsed.ToString();
        }

        private static void ClearEmpty(JContainer container, List<JToken> nodesToDelete)
        {
            if (container == null) return;

            foreach (var child in container.Children())
            {
                var cont = child as JContainer;

                if (child.Type == JTokenType.Property ||
                    child.Type == JTokenType.Object ||
                    child.Type == JTokenType.Array)
                {
                    if (child.HasValues)
                    {
                        ClearEmpty(cont, nodesToDelete);
                    }
                    else
                    {
                        nodesToDelete.Add(child.Parent);
                    }
                }
            }
        }
        #endregion

        #region Remove Caracter do JSON
        public static string RemoveCaracter(string stexto)
        {
            // texto = Regex.Replace(texto, "[\\(\\)\\-\\ ]", "");
            string pattern = @"(?i)[^0-9a-zA-Z ]";
            string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";
            string replacement = "", sAlterado = "";

            Regex rgx = new Regex(pattern);

            for (int i = 0; i < comAcentos.Length; i++)
            {
                stexto = stexto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            }

            sAlterado = rgx.Replace(stexto, replacement);

            return sAlterado.TrimStart(' ').TrimEnd(' ');
        }
        #endregion

        #region PERMITE IGNORAR PARAMETROS INDESEJADOS AO SERIALIZAR UM OBJETO
        public class IgnorePropertiesResolver : DefaultContractResolver
        {
            private readonly HashSet<string> ignoreProps;

            public IgnorePropertiesResolver(IEnumerable<string> propNamesToIgnore)
            {
                this.ignoreProps = new HashSet<string>(propNamesToIgnore);
            }

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty property = base.CreateProperty(member, memberSerialization);
                if (this.ignoreProps.Contains(property.PropertyName))
                {
                    property.ShouldSerialize = _ => false;
                }
                return property;
            }
        }
        #endregion

        #region CONVERTE DATATABLE PARA JSON
        public static string DataTable_JSON_StringBuilder(DataTable tabela)
        {
            var JSONString = new StringBuilder();
            if (tabela.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < tabela.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < tabela.Columns.Count; j++)
                    {
                        if (j < tabela.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + tabela.Columns[j].ColumnName.ToString() + "\":" + "\"" + tabela.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == tabela.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + tabela.Columns[j].ColumnName.ToString() + "\":" + "\"" + tabela.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == tabela.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }
        #endregion
    }
}