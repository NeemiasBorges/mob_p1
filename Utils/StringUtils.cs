﻿
using System.Text;


namespace Utils
{
    public static class StringUtils
    {
        /// <summary>
        /// Método responsável por remover caracteres especiais levantados pela Vero.
        /// </summary>
        /// <param name="str">string que deseja remover os caracteres.</param>
        /// <returns>Retorna uma nova string sem os caracteres removidos.</returns>
        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if (c == '°' || c == 'º' || c == 'ª')
                {
                    sb.Append(' ');
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}