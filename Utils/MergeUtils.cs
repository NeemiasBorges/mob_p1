﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utils
{
    public class MergeUtils
    {
        private static string[] LISTA_CAMPOS_N_ALTERAR = { "InternalCode", "RowNum", "InternalKey", "RowNumber", "BPCode" };

        /// <summary>
        /// CLASSE QUE RECEBE DOIS OBJETOS DO MESMO TIPO E FAZ UM 'MERGE' ENTRE ELES
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="target">TARGET(fonte): Objeto que sofrerá o 'merge'</param>
        /// <param name="source">SOURCE(alvo): Objeto que contêm a alteração</param>
        #region CLASSE QUE RECEBE DOIS OBJETOS DO MESMO TIPO E FAZ UM 'MERGE' ENTRE ELES | SOURCE(alvo): Objeto que contêm a alteração | TARGET(fonte): Objeto que sofrerá o 'merge'
        public void CopyValues<T>(T target, T source)
        {
            try
            {
                Type t = typeof(T);

                /* Verifica se o objeto alvo é nulo, caso sim, apenas replico o objeto fonte nele */
                if (target == null)
                {
                    target = source;
                }

                /* Verifica cada propriedade de meu objeto para poder controlar a troca de valores */
                var properties = t.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);

                foreach (var prop in properties)
                {
                    /* Desconsidera tudo que for lista filha */
                    if (prop.PropertyType.Name != "Collection`1")
                    {
                        var value = prop.GetValue(source, null);
                        /* Verifica se não é um campo que o valor não possa ser alterado, como um campo PK */
                        if (value != null && !LISTA_CAMPOS_N_ALTERAR.Contains(prop.Name))
                            prop.SetValue(target, value, null);

                        if (prop.Name == "RowNum")
                        {
                            var row = prop.GetValue(target, null);
                            prop.SetValue(target, row, null);
                        }

                        if (prop.Name == "InternalKey")
                        {
                            var row = prop.GetValue(target, null);
                            prop.SetValue(target, row, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        #endregion

        /// <summary>
        /// COMPARA DUAS LISTAS PARA REALIZAR O 'MERGE'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetList">targetList: Lista que sofrerá o 'merge'</param>
        /// <param name="sourceList">sourceList: Lista que contêm as alterações</param>
        /// <param name="selector">Integer usado como seletor nas listas</param>
        #region COMPARA DUAS LISTAS PARA REALIZAR O 'MERGE' | targetList: Lista que sofrerá o 'merge' | sourceList: Lista que contêm as alterações | selector INT
        public void MergeLists<T>(IList<T> targetList, IList<T> sourceList, Func<T, int> selector)
        {
            var matchingPrimaryKey = sourceList.Select(x => selector(x)).ToList();

            foreach (var thismatchingPrimaryKey in matchingPrimaryKey)
            {
                T target = targetList.SingleOrDefault(x => selector(x) == thismatchingPrimaryKey);
                T source = sourceList.SingleOrDefault(x => selector(x) == thismatchingPrimaryKey);

                if (target == null)
                    targetList.Add(source);
                else
                    CopyValues<T>(target, source);
            }
        }
        #endregion

        /// <summary>
        /// COMPARA DUAS LISTAS PARA REALIZAR O 'MERGE'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetList">targetList: Lista que sofrerá o 'merge'</param>
        /// <param name="sourceList">sourceList: Lista que contêm as alterações</param>
        /// <param name="selector">String usado como seletor nas listas</param>
        #region COMPARA DUAS LISTAS PARA REALIZAR O 'MERGE' | targetList: Lista que sofrerá o 'merge' | sourceList: Lista que contêm as alterações | selector STRING
        public void MergeLists<T>(IList<T> targetList, IList<T> sourceList, Func<T, string> selector)
        {
            var matchingPrimaryKey = sourceList.Select(x => selector(x)).ToList();

            foreach (var thismatchingPrimaryKey in matchingPrimaryKey)
            {
                T target = targetList.FirstOrDefault(x => selector(x) == thismatchingPrimaryKey);
                T source = sourceList.FirstOrDefault(x => selector(x) == thismatchingPrimaryKey);

                if (target == null)
                    targetList.Add(source);
                else
                    CopyValues<T>(target, source);
            }
        }
        #endregion

        /* Tratativas específicas para CRD7 */
        #region COMPARA DUAS LISTAS DE CRD7 PARA REALIZAR O 'MERGE' | targetList: Lista que sofrerá o 'merge' | sourceList: Lista que contêm as alterações | selector STRING
        public void MergeListsCRD7<T>(IList<T> targetList, IList<T> sourceList, Func<T, string> selector)
        {
            var matchingPrimaryKey = sourceList.Select(x => selector(x)).ToList();

            T target = targetList.SingleOrDefault(x => selector(x) == string.Empty);
            T source = sourceList.SingleOrDefault(x => selector(x) == null);

            CopyValuesCRD7<T>(target, source);

        }
        #endregion

        #region CLASSE QUE RECEBE DOIS OBJETOS DO MESMO TIPO CRD7 E FAZ UM 'MERGE' ENTRE ELES | SOURCE(fonte): Objeto que contêm a alteração | TARGET(alvo): Objeto que sofrerá o 'merge'
        public void CopyValuesCRD7<T>(T target, T source)
        {
            try
            {
                Type t = typeof(T);

                /* Verifica se o objeto alvo é nulo, caso sim, apenas replico o objeto fonte nele */
                if (target == null)
                {
                    target = source;
                }

                /* Verifica cada propriedade de meu objeto para poder controlar a troca de valores */
                var properties = t.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);

                foreach (var prop in properties)
                {
                    var value = prop.GetValue(source, null);
                    /* Verifica se não é um campo que o valor não possa ser alterado, como um campo PK */
                    if (prop.Name == "TaxId0")
                        prop.SetValue(target, value, null);

                    if (prop.Name == "TaxId4")
                    {
                        prop.SetValue(target, value, null); ;
                    }

                    if (prop.Name == "TaxId1")
                    {
                        prop.SetValue(target, value, null); ;
                    }

                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        #endregion

        #region COMPARA DUAS LISTAS PARA REALIZAR O 'MERGE', ONDE SÓ SERÁ FEITO MERGE CASO NÃO EXISTA NA LISTA ALVO. | targetList: Lista que sofrerá o 'merge' | sourceList: Lista que contêm as alterações | selector STRING, selector2 STRING
        public void MergeListsBPAccounts<T>(IList<T> targetList, IList<T> sourceList, Func<T, string> selector, Func<T, string> selector2)
        {

            var matchingPrimaryKey = sourceList.Select(x => selector(x)).ToList(); //array de chave primária
            var matchingSecondKey = sourceList.Select(x => selector2(x)).ToList(); //array de chave secundária

            for (int i = 0; i < sourceList.Count; i++)
            {
                T source = sourceList.SingleOrDefault(x => selector(x) == matchingPrimaryKey[i] && selector2(x) == matchingSecondKey[i]);
                T target = targetList.SingleOrDefault(x => selector(x) == matchingPrimaryKey[i] && selector2(x) == matchingSecondKey[i]);

                /* Caso não exista na lista alvo, será inserido */
                if (target == null)
                    targetList.Add(source);
                else
                    CopyValues<T>(target, source);
            }
        }
        #endregion
    }
}