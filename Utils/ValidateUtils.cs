﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Utils
{
    static class ValidateUtils
    {
        public static bool EmailValidate(string email)
        {
            Regex rg = new Regex(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((.(?!.))|[-!#$%&'*+/=?^`{}|~\w]))(?<=[0-9a-zA-Z])@))(?([)([(\d{1,3}.){3}\d{1,3}])|(([0-9a-zA-Z][-\w][0-9a-zA-Z].)+[a-zA-Z]{2,6}))$");

            bool bValid;
            if (rg.IsMatch(email))
            {
                bValid = true;
            }
            else
            {
                bValid = false;
            }

            return bValid;
        }
    }
}
