﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utils
{
    public class MessageUtils
    {
        public const string SUCESSO_INSERIDO = "Inserido com sucesso!";
        public const string SUCESSO_BAIXA = "Baixa dada com sucesso!";
        public const string SUCESSO_DEVOLUCAO = "Devolução realizada com sucesso!";
        public const string SUCESSO_ATUALIZACAO = "Atualizado com sucesso!";
        public const string SUCESSO_CANCELAMENTO = "Cancelado com sucesso!";
        public const string SUCESSO_CONEXAO = "Conectado com sucesso!";

        public const string ERRO_CONEXAO = "Erro ao conectar: ";
        public const string ERRO_EXCEPTION = "Ocorreu a seguinte exceção: ";
        public const string ERRO_INSERCAO = "Erro ao inserir documento: {0}. ";
        public const string ERRO_REGISTRO = "Erro ao inserir registro. ";

        public const string SESSAO_INVALIDA = "Invalid session.";

        public const string BAD_REQUEST = "400 Bad Request";

        public const string NOT_FOUND = "404 Not Found";
    }
}