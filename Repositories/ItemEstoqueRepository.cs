﻿using DataBase;
using PortalMOB.Repositories.Interfaces;
using Database;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Utils;

namespace PortalMOB.Repositories
{
    public class ItemEstoqueRepository : IItemEstoqueRepository
    {
        #region List
        public List<ItemEstoque> List()
        {
            List<ItemEstoque> list = new List<ItemEstoque>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT \"ItemCode\",\"ItemName\",\"InvntryUom\" as \"UnidadeMedida\",CASE WHEN \"ItemClass\" = '1'  THEN 'Serviço' ELSE 'Material' END \"ItemClass\"," +
                $"CASE WHEN \"U_P1_ITMCT\" = 'U'  THEN 'Uso e consumo' WHEN  \"U_P1_ITMCT\" = 'I'  THEN 'Instalável' WHEN \"U_P1_ITMCT\" = 'E' THEN 'Equipamento' END \"TipoItem\" FROM  {ConstantUtils.QUERYBASE}OITM");
            SQL.AppendLine("ORDER BY \"ItemName\" ASC ");

            try
            {
                list = Conexao.ConsultaBanco<List<ItemEstoque>>(SQL.ToString(),true);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return list;
        }
        #endregion

        #region Pesquisar item
        public List<ItemEstoque> Search(string name)
        {
            List<ItemEstoque> list = new List<ItemEstoque>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT TOP 30 \"ItemCode\",\"ItemName\",\"InvntryUom\",T1.\"ItmsGrpNam\",CASE WHEN OITM.\"U_P1_ITMCT\" = 'I' THEN 'Instalável' WHEN OITM.\"U_P1_ITMCT\" = 'E' THEN 'Equipamento' ELSE 'Consumivel' END \"ItemClass\" ");
            SQL.AppendLine($"FROM  {ConstantUtils.QUERYBASE}OITM ");
            SQL.AppendLine($"INNER JOIN  {ConstantUtils.QUERYBASE}OITB T1 ON (OITM.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\") ");
            SQL.AppendLine("WHERE (( UPPER(\"ItemName\") like '%" + name + "%')OR( UPPER(\"ItemCode\") like '%" + name + "%')) AND \"PrchseItem\" = 'Y' AND \"validFor\" = 'Y' ");
            SQL.AppendLine("ORDER BY \"ItemName\" ASC ");

            try
            {
                list = Conexao.ConsultaBanco<List<ItemEstoque>>(SQL.ToString(), true);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (Exception)
            {

            }

            return list;
        }
        #endregion

        //#region Pesquisar Item de documento específico
        //public List<ItemEstoque> SearchSpecific(string name, int type, int top = 30)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<ItemEstoque> list = new List<ItemEstoque>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine($"SELECT TOP {top} ");
        //    SQL.AppendLine("\"ItemCode\",\"ItemName\",\"InvntryUom\",T1.\"ItmsGrpNam\",CASE WHEN OITM.\"ItemClass\" = '1'  THEN 'Serviço' ELSE 'Material' END \"ItemClass\" ");
        //    SQL.AppendLine("FROM OITM ");
        //    SQL.AppendLine("INNER JOIN OITB T1 ON (OITM.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\") ");
        //    SQL.AppendLine("WHERE (( UPPER(\"ItemName\") like '%" + name.Trim() + "%')OR( UPPER(\"ItemCode\") like '%" + name.Trim() + "%')) AND \"PrchseItem\" = 'Y' AND \"validFor\" = 'Y' ");

        //    string defineTypeInSQL;
        //    #region Definindo o type na query           
        //    switch (type)
        //    {
        //        case 1:
        //            defineTypeInSQL = "AND \"QryGroup1\" = 'Y' ";
        //            break;
        //        case 6:
        //            defineTypeInSQL = "AND \"QryGroup6\" = 'Y' ";
        //            break;
        //        case 7:
        //            defineTypeInSQL = "AND \"QryGroup7\" = 'Y' ";
        //            break;
        //        case 16:
        //            defineTypeInSQL = "AND \"QryGroup16\" = 'Y' ";
        //            break;
        //        case 17:
        //            defineTypeInSQL = "AND \"QryGroup17\" = 'Y' ";
        //            break;
        //        case 18:
        //            defineTypeInSQL = "AND \"QryGroup18\" = 'Y' ";
        //            break;
        //        case 19:
        //            defineTypeInSQL = "AND \"QryGroup19\" = 'Y' ";
        //            break;
        //        default:
        //            defineTypeInSQL = "AND 1 = 1 ";
        //            break;
        //    }
        //    #endregion
            
        //    SQL.AppendLine(defineTypeInSQL);
                        
        //    SQL.AppendLine("ORDER BY \"ItemName\" ASC ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        ItemEstoque item = new ItemEstoque();
        //        item.ItemCode = oRecordset.Fields.Item("ItemCode").Value.ToString();
        //        item.ItemName = oRecordset.Fields.Item("ItemName").Value.ToString();
        //        item.UnidadeMedida = oRecordset.Fields.Item("InvntryUom").Value.ToString();
        //        item.GroupItem = oRecordset.Fields.Item("ItmsGrpNam").Value.ToString();
        //        item.ItemClass = oRecordset.Fields.Item("ItemClass").Value.ToString();
        //        list.Add(item);

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return list;
        //}
        //#endregion

        //#region Pesquisar do Financeiro
        //public List<ItemEstoque> SearchFinanceiro(string name)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<ItemEstoque> list = new List<ItemEstoque>();

        //    StringBuilder SQL = new StringBuilder();

        //    SQL.AppendLine("SELECT TOP 30 \"ItemCode\",\"ItemName\",\"InvntryUom\",T1.\"ItmsGrpNam\",CASE WHEN OITM.\"ItemClass\" = '1'  THEN 'Serviço' ELSE 'Material' END \"ItemClass\" ");
        //    SQL.AppendLine("FROM OITM ");
        //    SQL.AppendLine("INNER JOIN OITB T1 ON(OITM.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\") ");
        //    SQL.AppendLine("WHERE   ((UPPER(\"ItemName\") like '%" + name.Trim() + "%' AND \"PrchseItem\" = 'Y' AND \"SellItem\" = 'Y' AND \"validFor\" = 'Y') OR (UPPER(\"ItemCode\") like '%" + name.Trim() + "%' AND \"PrchseItem\" = 'Y' AND \"SellItem\" = 'Y' AND \"validFor\" = 'Y'))  ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        ItemEstoque item = new ItemEstoque();
        //        item.ItemCode      = oRecordset.Fields.Item("ItemCode").Value.ToString();
        //        item.ItemName      = oRecordset.Fields.Item("ItemName").Value.ToString();
        //        item.UnidadeMedida = oRecordset.Fields.Item("InvntryUom").Value.ToString();
        //        item.GroupItem     = oRecordset.Fields.Item("ItmsGrpNam").Value.ToString();
        //        item.ItemClass     = oRecordset.Fields.Item("ItemClass").Value.ToString();
        //        list.Add(item);

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return list;
        //}
        //#endregion
        
        //#region Pesquisar top 1 do Financeiro
        //public ItemEstoque SearchTopFinanceiro(string name)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    ItemEstoque item = new ItemEstoque();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT TOP 1 \"ItemCode\",\"ItemName\",\"InvntryUom\",T1.\"ItmsGrpNam\",CASE WHEN OITM.\"ItemClass\" = '1'  THEN 'Serviço' ELSE 'Material' END \"ItemClass\"  ");
        //    SQL.AppendLine("FROM OITM");
        //    SQL.AppendLine("INNER JOIN OITB T1 ON(OITM.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\") ");
        //    SQL.AppendLine("WHERE COALESCE(\"U_P1_ITMF\", '') = '" + name  + "' AND \"validFor\" = 'Y' ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        item.ItemCode      = oRecordset.Fields.Item("ItemCode").Value.ToString();
        //        item.ItemName      = oRecordset.Fields.Item("ItemName").Value.ToString();
        //        item.UnidadeMedida = oRecordset.Fields.Item("InvntryUom").Value.ToString();
        //        item.GroupItem     = oRecordset.Fields.Item("ItmsGrpNam").Value.ToString();
        //        item.ItemClass     = oRecordset.Fields.Item("ItemClass").Value.ToString();

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return item;
        //}
        //#endregion

        //#region Pesquisar itens de Estoque
        //public List<ItemEstoque> SearchOnly(string name)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<ItemEstoque> list = new List<ItemEstoque>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT TOP 30 \"ItemCode\",\"ItemName\",\"InvntryUom\",T1.\"ItmsGrpNam\",CASE WHEN OITM.\"ItemClass\" = '1'  THEN 'Serviço' ELSE 'Material' END \"ItemClass\" ");
        //    SQL.AppendLine("FROM OITM ");
        //    SQL.AppendLine("INNER JOIN OITB T1 ON (OITM.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\") ");
        //    SQL.AppendLine("WHERE (( UPPER(\"ItemName\") like '%" + name + "%')OR( UPPER(\"ItemCode\") like '%" + name + "%')) ");
        //    SQL.AppendLine("AND \"InvntItem\" = 'Y' AND \"validFor\" = 'Y' AND \"QryGroup1\" = 'Y' ");
        //    SQL.AppendLine("ORDER BY \"ItemName\" ASC ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        ItemEstoque item = new ItemEstoque();
        //        item.ItemCode = oRecordset.Fields.Item("ItemCode").Value.ToString();
        //        item.ItemName = oRecordset.Fields.Item("ItemName").Value.ToString();
        //        item.UnidadeMedida = oRecordset.Fields.Item("InvntryUom").Value.ToString();
        //        item.GroupItem = oRecordset.Fields.Item("ItmsGrpNam").Value.ToString();
        //        item.ItemClass = oRecordset.Fields.Item("ItemClass").Value.ToString();
        //        list.Add(item);

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return list;
        //}
        //#endregion

        //#region Pegar Quantidade Disponível por item
        //public float GetQtdDisponivelByItem(string ItemCode, string WhsCode)
        //{
        //    float disponivel = 0;

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"OnHand\", \"IsCommited\", \"OnOrder\" FROM OITW ");
        //    SQL.AppendLine("WHERE \"ItemCode\" = '"+ ItemCode + "' AND \"WhsCode\" = '" + WhsCode + "' ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    if (!oRecordset.EoF)
        //    {
        //        ItemEstoque item = new ItemEstoque();
        //        item.EmEstoque = float.Parse(oRecordset.Fields.Item("OnHand").Value.ToString());
        //        item.Confirmado = float.Parse(oRecordset.Fields.Item("IsCommited").Value.ToString());
        //        item.Pedido = float.Parse(oRecordset.Fields.Item("OnOrder").Value.ToString());
        //        item.Disponivel = (item.EmEstoque - item.Confirmado + item.Pedido);

        //        disponivel = item.EmEstoque;

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return disponivel;
        //}
        //#endregion
    }
}
