﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PowerOne.Database;
using PowerOne.Models;
using System.Text;
using DataBase;
using PortalMOB.Repositories.Interfaces;
using Utils;

namespace PortalMOB.Repositories
{
    public class CentroCustoRepository : ICentroCursoRepository
    {
        #region List Centros de Custo
        public List<CentroCusto> List(int DimCode)
        {
            List<CentroCusto> list = new List<CentroCusto>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT \"PrcCode\",\"PrcName\" FROM {ConstantUtils.QUERYBASE}\"OPRC\"  WHERE \"DimCode\" ='" + DimCode + "'");
            SQL.AppendLine("ORDER BY \"PrcName\" ");

            try
            {
                list = Conexao.ConsultaBanco<List<CentroCusto>>(SQL.ToString(), true);
            }
            catch (Exception)
            {

                throw;
            }
            return list;
        }
        #endregion

        //#region List Todos
        //public List<CentroCusto> ListAll()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<CentroCusto> list = new List<CentroCusto>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"PrcCode\",\"PrcName\" FROM OPRC  ");
        //    SQL.AppendLine("ORDER BY \"PrcName\" ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        CentroCusto ctCusto = new CentroCusto();
        //        ctCusto.OcrCode = oRecordset.Fields.Item("PrcCode").Value.ToString();
        //        ctCusto.OcrName = oRecordset.Fields.Item("PrcName").Value.ToString();

        //        list.Add(ctCusto);

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return list;
        //}
        //#endregion

        //#region List Centro de Custo por Autorização
        //public List<CentroCusto> ListByAuth(Login login)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<CentroCusto> centroCustos = new List<CentroCusto>();

        //    if (login.Autorizacoes.Tipo != "Adm")
        //    {
        //        StringBuilder SQL = new StringBuilder();
        //        SQL.AppendLine("SELECT T0.\"U_G2_FolhaJur\",C0.\"DocEntry\",C1.\"U_G2_CodCC\",C1.\"U_G2_CC\" FROM OHEM T0 ");
        //        SQL.AppendLine("INNER JOIN \"@G2_COMISSAO\" C0 ON (C0.\"DocEntry\" = T0.\"U_G2_FolhaJur\") ");
        //        SQL.AppendLine("INNER JOIN \"@G2_COM_PARAM\" C1 ON (C0.\"DocEntry\" = C1.\"DocEntry\") ");
        //        SQL.AppendLine($"WHERE T0.\"empID\" = {login.Id} ");
        //        SQL.AppendLine($"ORDER BY C1.\"U_G2_CC\" ");

        //        oRecordset.DoQuery(SQL.ToString());

        //        while (!oRecordset.EoF)
        //        {
        //            CentroCusto ctCusto = new CentroCusto();
        //            ctCusto.OcrCode = oRecordset.Fields.Item("U_G2_CodCC").Value.ToString();
        //            ctCusto.OcrName = oRecordset.Fields.Item("U_G2_CC").Value.ToString();

        //            centroCustos.Add(ctCusto);

        //            oRecordset.MoveNext();
        //        }
        //    }
        //    else
        //    {
        //        centroCustos = List(2);
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return centroCustos;
        //}
        //#endregion
    }
}