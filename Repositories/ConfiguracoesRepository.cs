﻿using DAL;
using DataBase;
using Newtonsoft.Json;
using PortalMOB.Models;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Helpers;
using Utils;
using static PowerOne.Util.Log;

namespace PowerOne.Repository
{
    public class ConfiguracoesRepository : IConfiguracoesRepository
    {
        #region Configurar Email
        public string configuraremail(ConfiguracoesEmail configuracoesemail)
        {
            string msg = null;
            string Header = ConstantUtils.URL_SERVICE_LAYER+"U_P1_EMAILCONF('1')";
            string Json = "{\"U_P1_Email\":\"" + configuracoesemail.Email + "\",\"U_P1_Senha\":\"" + configuracoesemail.Senha + "\",\"U_P1_Servidor\":\"" + configuracoesemail.Servidor + "\",\"U_P1_Porta\":\"" + configuracoesemail.Porta + "\"}";
            try
            {
                msg = RequestUtils.Request(Header, RequestUtils.PATCH, LoginDAL.sessionId, Json, false);
            }
            catch (Exception)
            {

            }

            return msg;
        }
        #endregion

        #region Configurar Anexo de Email
        public string ConfiguraAnexo(ConfiguracoesEmail configuracoesAnexo)
        {

            string msg = null;

            string Header = ConstantUtils.URL_SERVICE_LAYER+ "P1_EMAILCONF(1)";
            string Json = $"\"U_P1_DrBoleto\":\"{configuracoesAnexo.DirBoleto}\", \"U_P1_DrNtFisc\":\"{configuracoesAnexo.DirNotaFiscal}\"";

            try
            {
                msg = RequestUtils.Request(Header, RequestUtils.PATCH, LoginDAL.sessionId, Json, false);
            }
            catch (Exception e)
            {

                Log.Gravar(e.Message, Log.TipoLog.Erro);
            }
                

            return msg;
        }
        #endregion

        #region Pegar Configuracoes de Email
        public ConfiguracoesEmail GetConfiguracoesEmail()
        {

            ConfiguracoesEmail configEmail = null;

            StringBuilder SQl = new StringBuilder();

            SQl.AppendLine($"SELECT \"U_P1_Email\" \"Email\", \"U_P1_Senha\" \"Senha\", \"U_P1_Servidor\" \"Servidor\", \"U_P1_Porta\" \"Porta\" FROM {ConstantUtils.QUERYBASE}\"@P1_EMAILCONF\"");

            try
            {
                configEmail =  Conexao.ConsultaBanco<ConfiguracoesEmail[]>(SQl.ToString(), true)[0];
            }
            catch (Exception)
            {

                
            }
            

            return configEmail;
        }
        #endregion

        #region Configurações de Tema
        public Resultado ConfigurarTema(string code, string cor, string icone)
        {
            Resultado resultado = new Resultado();

            string resul = "";
            
            string jsonEnvio = "{\"Code\" : " + code + "";

            if (!String.IsNullOrEmpty(icone))
            {
                jsonEnvio +=  ",\"U_P1_Icones\" : \"" + icone + "\"";
            }
            if (!String.IsNullOrEmpty(cor))
            {
                jsonEnvio += ",\"U_P1_Color\":   \"" + cor + "\"";
            }
            jsonEnvio += "} ";

            try
            {
                resul = RequestUtils.Request(ConstantUtils.URL_SERVICE_LAYER + "U_P1_TEMA('" + code + "')", RequestUtils.PATCH, DAL.LoginDAL.sessionId, jsonEnvio, false);
            }
            catch (Exception e)
            {
                resultado.Msg = e.Message;
                resultado.Tipo= "error";
                Log.Gravar(resultado.Msg, Log.TipoLog.Erro);
            }

            //if (resultado.Msg == null)
            //{
            //    resultado.Msg = resul;
            //    resultado.Tipo = "success";
            //}

            return resultado;
        }
        #endregion



        #region definir deposito e filial padrão
        public string ConfDFPadrao(string idFilial, string idDeposito,bool ativado)
        {
            string resul = null;
            string Header = $"{ConstantUtils.URL_SERVICE_LAYER}U_P1_CONFDC('1')";
                string Json = null; 
            if (!ativado)
            {
                Json = "{\"U_P1_IDDPSTP\":\"" + idDeposito + "\", \"U_P1_IDFILIALP\":\"" + idFilial + "\"}";

            }
            else
            {
                Json = "{\"U_P1_IDDPSTP\":\"\", \"U_P1_IDFILIALP\":\"\"}";
            }

            try
            {
                resul = RequestUtils.Request(Header, RequestUtils.PATCH, DAL.LoginDAL.sessionId, Json, false);
            }
            catch (Exception)
            {

            }

            return resul;
        }
        #endregion

        #region Pegar Configuracoes de Tema
        public Tema GetTema(string code)
        {

            Tema configTema = null;
            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine($"SELECT \"Code\" as \"UserId\", COALESCE(\"U_P1_Color\",'RED') AS \"Color\", COALESCE(\"U_P1_Icones\",'PRO') AS \"Icon\" FROM {ConstantUtils.QUERYBASE}\"@P1_TEMA\"");
                SQL.AppendLine("WHERE \"Code\" = '" + code + "' FOR JSON PATH ");

                configTema = Conexao.ConsultaBanco<Tema[]>(SQL.ToString())[0];
            }
            catch (Exception)
            {
                configTema = JsonConvert.DeserializeObject<Tema>("{\"Color\" : 'RED', \"Icon\": 'PRO'}");
            }

            return configTema;

        }
        #endregion

        #region Definir Dimensões Centros de Custo
        public string ConfigurarDimencoes(int[] dimencoes)
        {
            string response = string.Empty;            

            try
            {                
                string U_P1_Ativado = string.Empty;

                for (int dimencao = 1; dimencao <= 5; dimencao++)
                {
                    U_P1_Ativado = dimencoes.Contains(dimencao) ? "Y" : "N";

                    string url = $"{ConstantUtils.URL_SERVICE_LAYER}Dimensions({dimencao})";

                    var json = new { U_P1_Ativado };

                    response = RequestUtils.Request(url, RequestUtils.PATCH, LoginDAL.sessionId, Json.Encode(json), false);
                }
            }
            catch (Exception e)
            {
                response = e.Message;
                Log.Gravar(e.Message, Log.TipoLog.Erro);
            }

            return response;
        }
        #endregion

        #region Pegar Dimenções Centro de Custo
        public int[] GetDimencoes()
        {
            int[] DimencoesArr = new int[5];

            List<CentroCusto> Dimencoes = new List<CentroCusto>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT T0.\"DimCode\" FROM {ConstantUtils.QUERYBASE}ODIM T0");
            SQL.AppendLine("WHERE T0.\"U_P1_Ativado\" = 'Y'");                   

            try
            {
                Dimencoes = Conexao.ConsultaBanco<List<CentroCusto>>(SQL.ToString(), true);

                int addDim = 0;

                foreach (var dimmencao in Dimencoes)
                {
                    DimencoesArr[addDim] =dimmencao.DimCode;

                    addDim++;
                }
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return DimencoesArr;
        }
        #endregion            

        #region Pegar Configurações de valores Padrões
        public ConfDocumento GetConfigurationsOfDocuments()
        {
            ConfDocumento confDocumento = new ConfDocumento();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT * FROM {ConstantUtils.QUERYBASE}\"@P1_CONFDC\" ");
            SQL.AppendLine("WHERE \"Code\" = '1' ");

            try
            {
                confDocumento = Conexao.ConsultaBanco<ConfDocumento>(SQL.ToString());
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return confDocumento;
        }
        #endregion        
    }
}
