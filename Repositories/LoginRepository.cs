﻿using DataBase;
using Models;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.ViewModel;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Timers;
using static PowerOne.Util.Log;
using Newtonsoft.Json;
using Utils;
using DAL;
using System.Web.Helpers;

namespace PowerOne.Repository
{
    public class LoginRepository : ILoginRepository
    {
        #region Login
        public Login Login(Login login)
        {
            Login user = new Login();

            try
            {
                StringBuilder SQL = new StringBuilder();

                SQL.AppendLine("SELECT TOP 1 T0.\"empID\" as \"UserId\",OBN.\"BinCode\" AS \"BinCode\",OBN.\"WhsCode\" AS \"WhsCode\",OBN.\"AbsEntry\" AS \"AbsEntry\", concat(concat(T0.\"firstName\", '.'), T0.\"lastName\") AS \"User\",");
                SQL.AppendLine("coalesce(T0.\"BPLId\", 1) \"IdFilial\", coalesce(T0.\"email\", '') \"email\", coalesce(T0.\"userId\", -1) \"Id\"");
                SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"OHEM\" T0 ");
                SQL.AppendLine($"INNER JOIN {ConstantUtils.QUERYBASE}\"@P1_ACCESS\" T1 ON T1.\"U_P1_EMPId\" = T0.\"empID\"");
                SQL.AppendLine($"LEFT JOIN {ConstantUtils.QUERYBASE}\"OBIN\" OBN ON T1.\"U_P1_EMPId\" = OBN.\"SL1Code\"");
                SQL.AppendLine("WHERE \"email\" = '" + login.email + "' AND COALESCE(T0.\"U_P1_STPORTAL\",'') != 'N' ");
                SQL.AppendLine("AND(T1.\"U_P1_Password\" = '" + Criptografia.CalculateSHA1(login.Password) + "' OR");
                SQL.AppendLine("(T1.\"U_P1_Password\" = 'NOVO' and T1.\"U_P1_Password\" = '" + login.Password + "')) FOR JSON PATH");

                user = Conexao.ConsultaBanco<Login[]>(SQL.ToString())[0];
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message,TipoLog.Erro);
            }

            return user;
        }
        #endregion

        #region Alterar Senha
        public void EditarSenha(AlterarSenhaViewModel alterarSenha)
        {
            try
            {
                string url = $"{ConstantUtils.URL_SERVICE_LAYER}U_P1_ACCESS('{alterarSenha.UserId}')";

                object json = new { U_P1_Password = Criptografia.CalculateSHA1(alterarSenha.NewPassword) };

                string response = RequestUtils.Request(url, RequestUtils.PATCH, LoginDAL.sessionId, Json.Encode(json), false);

                if (response != "Atualizado com sucesso!")
                    throw new Exception(response);
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }            
        }
        #endregion
    
        #region Pegar Usuários por: Primeiro Nome, Últim Nome e Email
        public string GetIdUsuarioPorEmail(string email)
        {
            string empID = null;
            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine($"SELECT \"empID\" FROM {ConstantUtils.QUERYBASE}OHEM ");
                SQL.AppendLine("WHERE \"email\" = '" + email + "' ");

                empID = Conexao.ConsultaBanco<string>(SQL.ToString());
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return empID;
        }
        #endregion            
        
        #region Pegar endereço MAC
        public string GetMacAddress()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
            string chars = "ABCDEFGHIJKM";
            string pass = "";
            Random random = new Random();
            for (int f = 0; f < 4; f++)
            {
                pass += chars.Substring(random.Next(0, chars.Length - 1), 1);
            }
            string aux1 = DateTime.Now.ToString(CultureInfo.InvariantCulture).Trim();
            string tratado = aux1.Replace("/", "").Trim().Replace(":", "").Trim().Replace(" ", "");
            string code = String.Concat(pass, tratado.Remove(0, 6));
            code = code.Substring(0, 12);
            return code;
        }
        #endregion

        #region Consulta Id do Colaborador 
        public int ConsultaId(Login login)
        {
            Login user = null;

            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine($"SELECT \"empID\" AS \"Id\" FROM {ConstantUtils.QUERYBASE}\"OHEM\" WHERE \"email\" = '" + login.email + "' FOR JSON PATH");

                user = Conexao.ConsultaBanco<Login[]>(SQL.ToString())[0];
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return user.Id;
        }
        #endregion

        #region Consulta Id do Colaborador P1_ACCESS
        public bool ConsultaCadastro(int idUser)
        {
            DataTable user = null;
            bool Valid = false;
       
            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine($"SELECT COALESCE(\"U_P1_EMPId\",0) as \"Id\" FROM {ConstantUtils.QUERYBASE}\"@P1_ACCESS\" ");
                SQL.AppendLine("WHERE \"U_P1_EMPId\" = '" + idUser + "' FOR JSON PATH");

                user = Conexao.ConsultaBanco(SQL.ToString());

                if (user.Rows.Count != 0)
                {
                    Valid = true;
                }
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return Valid;
        }
        #endregion
        
        #region Add valor caso nao exista 
        public string AddAcess(Login login, int idUser)
        {
            string msg = null;
            string jsonEnvio = "{\"Code\":   " + idUser + ",\"Name\" : " + idUser + ",\"U_P1_EMPId\": " + idUser + ",\"U_P1_Password\" : \"NOVO\"}";

            try
            {
                msg = RequestUtils.Request(ConstantUtils.URL_P1_ACESS, RequestUtils.POST, LoginDAL.sessionId, jsonEnvio, false);
                //Request(string url, string metodoEnvio, string sessionId, string jsonEnvio, bool sAddHeaderPatch)
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion   

        //#region Atualizar Senha de Usuários já cadastrados
        //public void ToFillTheEmptyPassword()
        //{
        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");
        //    //string senhaCript = "NOVO";
        //    //senhaCript += Criptografia.Encrypt(senhaCript, true);
        //    List<Models.Login> users = GetUserEmptyPasswords();

        //    if (users != null)
        //    {
        //        foreach (var user in users)
        //        {
        //            if (oAccess.GetByKey(user.Id.ToString()))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_Password").Value = "NOVO";

        //                int iRetcode = oAccess.Update();

        //                if (iRetcode != 0)
        //                {
        //                    string sErrMessage = oCompany.GetLastErrorDescription();
        //                }
        //            }

        //            //System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //            //oAccess = null;
        //        }
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //    oAccess = null;

        //}

        //public List<Models.Login> GetUserEmptyPasswords()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<Models.Login> userLogin = new List<Models.Login>();

        //    string SQL = "SELECT \"U_P1_EMPId\" FROM \"@P1_ACCESS\" WHERE \"U_P1_Password\" IS NULL";

        //    oRecordset.DoQuery(SQL);

        //    while (!oRecordset.EoF)
        //    {
        //        //oRecordset.MoveFirst();
        //        userLogin.Add(new Models.Login()
        //        {
        //            Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString())
        //        });

        //        oRecordset.MoveNext();
        //    }

        //    //if (!oRecordset.EoF)
        //    //{
        //    //    oRecordset.MoveFirst();
        //    //    userLogin.Add(new Models.Login()
        //    //    {
        //    //        Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString()),
        //    //        User = oRecordset.Fields.Item(1).Value.ToString()
        //    //    });
        //    //}
        //    //else
        //    //{
        //    //    userLogin = null;
        //    //}

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return userLogin;
        //}

        //#endregion
    }
}