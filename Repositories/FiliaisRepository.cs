﻿using System.Collections.Generic;
using PowerOne.Database;
using PowerOne.Models;
using System.Text;
using Models;
using PortalMOB.Repositories.Interfaces;
using DataBase;
using System;
using Utils;

namespace PortalMOB.Repositories
{
    public class FiliaisRepository : IFiliaisRepository
    {
        #region List
        public List<Filiais> List()
        {

            List<Filiais> list = new List<Filiais>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT \"BPLId\", \"BPLName\", \"AliasName\"  FROM {ConstantUtils.QUERYBASE}OBPL WHERE NOT \"BPLId\" = 2 AND \"Disabled\" = 'N' ORDER BY \"BPLName\"");

            try
            {
                list = Conexao.ConsultaBanco<List<Filiais>>(SQL.ToString(), true);
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }


            return list;
        }
        #endregion

        #region Search
        //public List<Filiais> Search(string name)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            
        //    List<Filiais> list = new List<Filiais>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"BPLId\", \"BPLName\",\"AliasName\" FROM OBPL WHERE NOT \"BPLId\" = 2 AND \"Disabled\" = 'N' ");
        //    SQL.AppendLine("AND ((\"BPLId\" like '%" + name.Trim() + "%') OR(\"AliasName\" like '%" + name.Trim() + "%')) ORDER BY \"BPLId\"");

        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        Filiais item = new Filiais();
        //        item.BplId = int.Parse(oRecordset.Fields.Item("BPLId").Value.ToString());
        //        item.BplName = oRecordset.Fields.Item("AliasName").Value.ToString();
        //        list.Add(item);

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return list;
        //}
        #endregion
    }
}