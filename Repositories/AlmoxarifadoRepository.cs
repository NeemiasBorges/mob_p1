﻿using DAL;
using DataBase;
using Microsoft.Ajax.Utilities;
using Model.Estoque;
using Models.Estoque;
using Newtonsoft.Json;
using PortalMOB.Models.Estoque;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Models;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Web;
using Utils;
using static PowerOne.Util.Log;
using static Utils.JsonUtils;
using PortalMOB.Database;

namespace PortalMOB.Repositories
{
    public class AlmoxarifadoRepository : IAlmoxarifadoRepository
    {

        #region serviços
        PowerOne.Controllers.EnvioEmail envio = new PowerOne.Controllers.EnvioEmail();
        EstoqueRepository estoque = new EstoqueRepository();
        #endregion


        #region search list
        public List<RequerimentoEstoque> List(
             Login login, string dtInicio, string dtFim, string status, int tipo
          )
        {
            List<RequerimentoEstoque> requerimentosEstoqueList = new List<RequerimentoEstoque>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", COALESCE(T0.\"U_P1_IDFILIAL\", 0) \"U_P1_IDFILIAL\", ");
            SQL.AppendLine("CASE WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'O'  THEN 'Aberta'" +
                " WHEN T0.\"U_P1_STATUS\" = 'C' THEN 'Cancelado' " +
                " WHEN T0.\"U_P1_STATUS\" = 'A' THEN 'Aprovado' " +
                "WHEN T0.\"U_P1_STATUS\" = 'P' THEN 'Em Andamento' " +
                "WHEN T0.\"U_P1_STATUS\" = 'R' THEN 'Aguardando Retirada' " +
                "WHEN T0.\"U_P1_STATUS\" = 'Y' THEN 'Concluído' END \"U_P1_STATUS\",");
            SQL.AppendLine("COALESCE(T0.\"U_P1_DTLANC\",'01/01/2021') \"U_P1_DTLANC\", ");
            SQL.AppendLine("CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"U_P1_Owner\" ");
            SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"@P1_OIGE\" T0 INNER JOIN {ConstantUtils.QUERYBASE}OHEM H ");
            SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");

            if (login.Autorizacoes.ReqEstoque != "T" && login.Autorizacoes.Grupo.ReqEstoque != "T")
                SQL.AppendLine("WHERE (T0.\"U_P1_Owner\" = '" + login.UserId.ToString() + "') ");
            else
                SQL.AppendLine("WHERE 1 = 1 ");

            #region Filtros

            if (!String.IsNullOrEmpty(dtInicio))
                SQL.AppendLine("AND T0.\"U_P1_DTLANC\" >= '" + (DateTime.ParseExact(dtInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' ");

            if (!String.IsNullOrEmpty(dtFim))
                SQL.AppendLine("AND T0.\"U_P1_DTLANC\" <='" + (DateTime.ParseExact(dtFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' ");

            if (tipo == 3)
            {
                SQL.AppendLine("AND T0.\"U_P1_STATUS\" = 'A' ");
            }
            else if (tipo == 5)
            {
                SQL.AppendLine("AND T0.\"U_P1_STATUS\" = 'Y' OR T0.\"U_P1_STATUS\" = 'R' ");
            }
            else
            {
                SQL.AppendLine("AND T0.\"U_P1_STATUS\" = 'P' ");

            }

            #endregion

            SQL.AppendLine("ORDER BY T0.\"U_P1_DTLANC\" DESC");
            //SQL.AppendLine("FOR JSON PATH ");

            try
            {
                requerimentosEstoqueList = Conexao.ConsultaBanco<List<RequerimentoEstoque>>(SQL.ToString(), true);
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return requerimentosEstoqueList;

        }

        #endregion

        #region atualiza status
        public string AttReq(int DocEntry, string status)
        {
            string result = null;

            //O - Aberto
            //A - Aprovada
            //P - Em Curso
            //R - Aguardando Retirada
            //Y - Concluido
            //C - Cancelada

            try
            {

                string urlHeader = ConstantUtils.URL_SERVICE_LAYER + "P1_OIGE(" + DocEntry + ")";
                string Json = "{\"DocEntry\":\"" + DocEntry + "\", \"U_P1_STATUS\":\"" + status + "\"}";

                result = RequestUtils.Request(urlHeader, RequestUtils.PATCH, LoginDAL.sessionId, Json, false);

                if (!result.Contains("Erro:"))
                {

                    if (status == "R")

                    {
                        List<Login> responsaveis = getResponsavelSetor();
                        List<String> itens = new List<String>();
                        RequerimentoEstoque requerimento = estoque.GetByKey(DocEntry);

                        foreach (var item in requerimento.P1_IGE1Collection)
                        {
                            itens.Add("<tr style=\"text-align:center;\"><td>" + item.U_P1_Quantity + " </td><td> " + item.U_P1_ItemCode + " </td></tr>");
                        }

                        foreach (var responsavel in responsaveis)
                        {
                            if (!String.IsNullOrEmpty(responsavel.email))
                            {
                                envio.EnviaEmail(responsavel.email, 3, responsavel.User, requerimento.DocNum.ToString(), itens);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return result;
        }
        #endregion

        #region gera documento final
        public string FecharRequerimento(RequerimentoEstoque requerimento, Login login, string status)
        {
            string result = null;
            string urlHeader = null;
            string Json = null;
            int Tipo = 0;
            int linhasFechadas = 0;
            //FAZ VERIFICAÇÃO DO ITEM, DE ACORDO COM A CATEGORIA DO ITEM O DOCUMENTO FINAL SERA DIFERENCIADO
            //    • Consumíveis – gera uma saída de mercadoria dos itens e quantidades.
            //    • Instaláveis – Gera uma transferência de stock para a localização do técnico.
            //    • Equipamentos
            //    o Modifica a localização do no ativo.
            //    o Caso seja um equipamento novo faz a sua capitalização

            var groupedCustomerList = requerimento.P1_IGE1Collection.GroupBy(u => u.U_P1_ITMCT).Select(grp => grp.ToList()).ToList(); /* uso consulta via c# para separar itens de grupos diferentes*/

            foreach (var documento in groupedCustomerList)
            {
                List<RequerimentoEstoqueLinhas> linhasGrupo = new List<RequerimentoEstoqueLinhas>();
                RequerimentoEstoque documentoNovo = new RequerimentoEstoque();

                foreach (var item in documento)
                {
                    linhasGrupo.Add(item);
                    if (item.U_P1_StatusLinha == "C")
                    {
                        Tipo = 0;
                    }
                    else if (item.U_P1_ITMCT == "Consumivel") /*Item de utilização*/
                    {
                        Tipo = 1;
                        urlHeader = ConstantUtils.URL_SERVICE_LAYER + "InventoryGenExits";
                    }
                    else if (item.U_P1_ITMCT == "Instalável")/* item instalavel*/
                    {
                        Tipo = 2;
                        urlHeader = ConstantUtils.URL_SERVICE_LAYER + "StockTransfers";
                    }
                    else
                    {
                        Tipo = 3;
                        urlHeader = ConstantUtils.URL_SERVICE_LAYER + "AssetCapitalization";

                        //Item Equipamento -  ativo

                    }
                }

                documentoNovo = requerimento;
                documentoNovo.P1_IGE1Collection = linhasGrupo;

                if (Tipo == 1)
                {
                    Json = REQSAIDA(documentoNovo); //monta o objeto de requesição de saida
                }
                else if (Tipo == 2)
                {
                    Json = TRANSFESTQ(documentoNovo, login); //monta o objeto de transferencia de estoque
                }
                else if (Tipo == 3)
                {
                    List<RequerimentoEstoqueLinhas> linhasAtivo = new List<RequerimentoEstoqueLinhas>();
                    RequerimentoEstoque docAtivo = new RequerimentoEstoque();

                    foreach (var item in documentoNovo.P1_IGE1Collection) /*verificase o item de ativo é um item novo ou ja utilizado, caso for novo, ira criar um documento de capitalização com os itens novos*/
                    {
                        if (!verificaNovo(item))
                        {
                            linhasAtivo.Add(item);
                        }
                    }

                    docAtivo = requerimento;
                    docAtivo.P1_IGE1Collection = linhasAtivo;

                    Json = ITEMATIVO(docAtivo, login); //monta o objeto de ITEM ATIVO
                }
                if (Tipo != 0)
                {

                    try
                    {
                        result = RequestUtils.Request(urlHeader, RequestUtils.POST, LoginDAL.sessionId, Json, false);
                    }
                    catch (Exception)
                    {

                    }
                    if (result.Contains("Erro:"))
                    {
                    }
                    else
                    {
                        linhasFechadas += FechaLinha(linhasGrupo, requerimento.DocEntry); /*caso não existam error na hora de fazer documento para a linha, codigo ira fechar o status da linha*/
                        result = "Concluido com sucesso";
                    }

                }
                if (linhasFechadas == groupedCustomerList.Count())
                {
                    FechaSME(requerimento.DocEntry.ToString());
                    RegistraHist(requerimento);
                }
            }

            return result;
        }

        #endregion

        #region verificaNovo
        public bool verificaNovo(RequerimentoEstoqueLinhas linha)
        {
            bool valid = false;

            Object teste = new { ItemCode = "" };
            
            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT TOP 1 \"ItemCode\" FROM {ConstantUtils.QUERYBASE}ACQ1 WHERE \"ItemCode\" = '{linha.U_P1_ItemCode}'");

            try
            {
                teste = Conexao.ConsultaBanco<Object>(SQL.ToString(), true);
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }
            if (teste != null)
            {
                string aaa = teste.ToString();
                if (aaa.Contains("ItemCode"))
                {
                    valid = true;
                }
            }

            return valid;
        }
        #endregion

        #region Fechar Linha
        public int FechaLinha(List<RequerimentoEstoqueLinhas> linhas, int DocEntry)
        {
            string urlHeader = ConstantUtils.URL_SERVICE_LAYER + "P1_OIGE(" + DocEntry + ")";
            string Json = "{\"P1_IGE1Collection\" : ";
            Json += JsonConvert.SerializeObject(linhas,
              new JsonSerializerSettings()
              {
                  ContractResolver = new IgnorePropertiesResolver(new[] { "U_P1_WhsName", "U_P1_ITMCT" })
              }
            );


            Json += "}";
            Json = Json.Replace("\"U_P1_StatusLinha\":\"O\"", " \"U_P1_StatusLinha\":\"C\"");

            try
            {
                string resul = RequestUtils.Request(urlHeader, RequestUtils.PATCH, LoginDAL.sessionId, Json, false);
            }
            catch (Exception)
            {

            }

            return linhas.Count();
        }
        #endregion

        #region fecha SME

        public void FechaSME(string DocEntry)
        {
            try
            {

                string UrlHeader = ConstantUtils.URL_SERVICE_LAYER + "P1_OIGE(" + DocEntry + ")";
                string Json = "{\"U_P1_STATUS\":\"Y\"}";

                string result = RequestUtils.Request(UrlHeader, RequestUtils.PATCH, LoginDAL.sessionId, Json, false);
            }
            catch (Exception)
            {


            }
        }

        #endregion

        #region ITEMATIVO
        public string ITEMATIVO(RequerimentoEstoque requerimento, Login login)
        {
            string resul = null;
            int virgulaFim = 1;
            int quantLinha = requerimento.P1_IGE1Collection.Count();
            resul = "" +
               "{" +
                   "\"PostingDate\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
                   "\"DocumentDate\": \"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
                   "\"Status\": \"adsPosted\"," +
                   "\"Currency\": \"R$\"," +
                   "\"AssetValueDate\": \"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
                   "\"DocumentType\": \"adtOrdinaryDepreciation\"," +
                   "\"SummerizeByProjects\": \"tNO\"," +
                   "\"SummerizeByDistributionRules\": \"tNO\"," +
                   "\"HandWritten\": \"tNO\"," +
                   "\"DepreciationArea\": \"*\"," +
                   "\"BPLId\": " + requerimento.U_P1_IDFILIAL + "," +
                   "\"LowValueAssetRetirement\": \"tNO\"," +
                   "\"CancellationOption\": \"coByCurrentSystemDate\"," +
                   "\"OriginalType\": \"aotCapitalization\"," +
                       "\"AssetDocumentLineCollection\":" +
                           "[";
            foreach (var linha in requerimento.P1_IGE1Collection)
            {
                resul +=
                               "{" +
                    "\"AssetNumber\": \"" + linha.U_P1_ItemCode + "\"," +
                    "\"Quantity\": 0.0," +
                    "\"TotalLC\": 5.0," +
                    "\"TotalFC\": 0.0," +
                    "\"TotalSC\": 5.0," +
                    "\"Partial\": \"tNO\"," +
                    "\"DistributionRule\": null, " +
                    "\"DistributionRule2\": null," +
                    "\"DistributionRule3\": null," +
                    "\"DistributionRule4\": null," +
                    "\"DistributionRule5\": null" +
                //if (virgulaFim == requerimento.P1_IGE1Collection.Count())
                //{
                //    resul += ",";
                //}
                    "}";
                if (quantLinha != 1 && virgulaFim != quantLinha - 1)
                {
                    resul += ",";
                }
                virgulaFim++;
            }
            resul += "]" +
           //"\"AssetDocumentAreaJournalCollection\": " +
           //"["+
           //    "{"+
           //    "\"DocEntry\": \"1,\""+
           //    "\"LineNumber\": \"1,\""+
           //    "\"DepreciationArea\": \"001\","+
           //    "\"JournalRemarks\": \"Capitalização\","+
           //    "\"TransactionNumber\": \"652\","+
           //    "\"CancellationJournalRemarks\": \"null\","+
           //    "\"CancellationTransactionNumber\": \"null\" "+
           //    "}"+
           //"]"+
           "}";


            return resul;
        }
        #endregion

        #region tranforma em req Saida
        public string REQSAIDA(RequerimentoEstoque requerimento)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);

            string teste = "{ ";
            int lineNum = 0;
            int quantLinha = requerimento.P1_IGE1Collection.Count();
            teste += "\"DocType\": \"dDocument_Items\"," +
                "\"DocDate\": \"" + DateTime.UtcNow + "\"," +
                "\"DocDueDate\": \"" + DateTime.UtcNow + "\"," +
                "\"Reference2\": \"" + requerimento.DocNum + "\"," +
                "\"BPL_IDAssignedToInvoice\":" + requerimento.U_P1_IDFILIAL + "," +
                "\"DocumentLines\": [";
            foreach (var item in requerimento.P1_IGE1Collection)
            {
                teste += "{" +
                    "\"LineNum\": \"" + lineNum + "\"," +
                    "\"ItemCode\": \"" + item.U_P1_ItemCode + "\"," +
                    //"\"ItemDescription\": \"Item Teste Via SL\"," +
                    "\"Quantity\": " + Double.Parse(item.U_P1_Quantity) + ", " +
                    "\"SerialNum\": null," +
                    "\"WarehouseCode\": \"" + item.U_P1_WhsCode + "\"," +
                    "\"SerialNumbers\": []," +
                    "\"BatchNumbers\": []," +
                    "\"CCDNumbers\": []," +
                    "\"DocumentLinesBinAllocations\": []" +
                 "}";
                if (quantLinha != 1 && lineNum != quantLinha - 1)
                {
                    teste += ",";
                }
                lineNum++;
            }

            teste += "]" +
                "}";

            return teste;
        }
        #endregion

        #region tranforma em transferencia de estoque
        public string TRANSFESTQ(RequerimentoEstoque requerimento, Login login)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
            string teste = "{ ";
            int lineNum = 0;
            int quantLinha = requerimento.P1_IGE1Collection.Count();

            string IDWhs = "";

            foreach (var linha in requerimento.P1_IGE1Collection)
            {
                IDWhs = linha.U_P1_WhsCode;
            }

            teste +=
            "\"DocDate\": \"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
            "\"DueDate\": \"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
            "\"JournalMemo\": \"" + requerimento.U_P1_Comments + "\", " +
            "\"FromWarehouse\": \"" + IDWhs + "\", " +
            "\"ToWarehouse\": \"" + login.WhsCode+ "\"," +
            "\"CreationDate\": \"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
            "\"TaxDate\": \"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"," +
            "\"BPLID\": 1," +
            "\"StockTransferLines\": [";
            foreach (var item in requerimento.P1_IGE1Collection)
            {
                teste += "{" +
                        "\"ItemCode\": \"" + item.U_P1_ItemCode + "\"," +
                        "\"Quantity\": " + Double.Parse(item.U_P1_Quantity) + "," +
                        "\"SerialNumber\": null," +
                        "\"WarehouseCode\": \"" + login.WhsCode + "\"," +
                        "\"FromWarehouseCode\": \"" + IDWhs + "\"," +
                        "\"BaseType\": \"Default\"," +
                        "\"InventoryQuantity\": " + Double.Parse(item.U_P1_Quantity) + "," +
                        "\"LineStatus\": \"bost_Open\"," +
                        "\"SerialNumbers\": []," +
                        "\"BatchNumbers\": []," +
                                 "\"StockTransferLinesBinAllocations\": [" +
                                    "{" +
                                        "\"BinAbsEntry\": " + login.AbsEntry + "," +
                                        "\"SerialAndBatchNumbersBaseLine\": -1,"+
                                        "\"Quantity\": " + Double.Parse(item.U_P1_Quantity) + "," +
                                        "\"AllowNegativeQuantity\": \"tNO\"," +
                                        "\"BaseLineNumber\": "+ lineNum +","+
                                        "\"BinActionType\": \"batToWarehouse\"" +
                                    "}" +
                                "]" +
                        "}";
                if (requerimento.P1_IGE1Collection.Count() != 1 && lineNum != requerimento.P1_IGE1Collection.Count - 1)
                {
                    teste += ",";
                }
                lineNum++;
            }
            teste += "]}";

            return teste;
        }
        #endregion

        #region verificaOBIN
        public List<OBIN> verificaOBIN(Login login)
        {
            List<OBIN> lista = new List<OBIN>();

            return lista;
        }
        #endregion

        #region pega responsavel pelo setor de almoxarifado

        public List<Login> getResponsavelSetor()
        {
            List<Login> respSetor = new List<Login>();
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT concat(concat(T0.\"firstName\", '.'), T0.\"lastName\") AS \"User\",");
            SQL.AppendLine("coalesce(T0.\"email\", '') \"email\"");
            SQL.AppendLine("FROM OHEM T0");
            SQL.AppendLine($"INNER JOIN {ConstantUtils.QUERYBASE}\"@P1_ACCESS\" T1 ON T1.\"U_P1_EMPId\" = T0.\"empID\"");
            SQL.AppendLine("WHERE T1.\"U_P1_ALMOX\" = 'T'");

            try
            {
                respSetor = Conexao.ConsultaBanco<List<Login>>(SQL.ToString(), true);
            }
            catch (Exception)
            {
                throw;
            }

            return respSetor;
        }

        #endregion

        #region RegistraHist
        public void RegistraHist(RequerimentoEstoque req)
        {
            string urlHeader = $"{ConstantUtils.URL_SERVICE_LAYER}U_P1_LOGMOB";

            string Json = "";

            foreach (var item in req.P1_IGE1Collection)
            {
                Json += "{\"Name\":\"" + req.DocEntry + "\",\"U_P1_LocAtual\":\"Técnico\",\"U_P1_Item\":\"" + item.U_P1_ItemCode + "\",\"U_P1_Aprovador\":\"" + req.U_P1_APROVADOR.Replace(".", " ") + "\",\"U_P1_LocAntes\":\"" + item.U_P1_WhsName + "\",\"U_P1_DtLog\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "\"}";
                try
                {
                    string result = RequestUtils.Request(urlHeader, RequestUtils.POST, LoginDAL.sessionId, Json, false);
                }
                catch (Exception)
                {

                }
            }

        }
        #endregion
    }
}


//{
//    "odata.metadata": "https://192.168.1.86:30030/b1s/v1/$metadata#StockTransfers/@Element",
//    "DocEntry": 4,
//    "Series": 21,
//    "DocDate": "2021-06-10",
//    "DueDate": "2021-06-10",
//    "Address": ",\r\r--\rBRASIL",
//    "FromWarehouse": "01",
//    "ToWarehouse": "042",
//    "StockTransferLines": [
//        {
//            "LineNum": 0,
//            "DocEntry": 4,
//            "ItemCode": "D001",
//            "Quantity": 1.0,
//            "SerialNumbers": [],
//            "BatchNumbers": [],
//            "CCDNumbers": [],
//            "StockTransferLinesBinAllocations": [
//                {
//                    "BinAbsEntry": 3,
//                    "Quantity": 1.0,
//                    "AllowNegativeQuantity": "tNO",
//                    "SerialAndBatchNumbersBaseLine": -1,
//                    "BinActionType": "batToWarehouse",
//                    "BaseLineNumber": 0
//                }
//            ]
//        },
//        {
//            "LineNum": 1,
//            "DocEntry": 4,
//            "ItemCode": "D002",
//            "Quantity": 1.0,
//            "SerialNumbers": [],
//            "BatchNumbers": [],
//            "CCDNumbers": [],
//            "StockTransferLinesBinAllocations": [
//                {
//                    "BinAbsEntry": 3,
//                    "Quantity": 1.0,
//                    "AllowNegativeQuantity": "tNO",
//                    "SerialAndBatchNumbersBaseLine": -1,
//                    "BinActionType": "batToWarehouse",
//                    "BaseLineNumber": 1
//                }
//            ]
//        }
//    ],
//    "StockTransferTaxExtension": {
//        "SupportVAT": "tNO",
//        "FormNumber": null,
//        "TransactionCategory": null
//    }
//}