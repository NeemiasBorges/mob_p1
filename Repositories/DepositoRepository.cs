﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PortalMOB.Database;
using Models;
using System.Text;
using DataBase;
using PowerOne.Util;
using static PowerOne.Util.Log;
using PortalMOB.Repositories.Interfaces;
using Utils;

namespace PortalMOB.Repositories
{
    public class DepositoRepository : IDepositoRepository
    {
        #region List
        public List<Deposito> List(int filial)
        {
        
            List<Deposito> list = new List<Deposito>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT \"WhsCode\", \"WhsName\" FROM {ConstantUtils.QUERYBASE}\"OWHS\" WHERE \"BPLid\" =" + filial + "");

            try
            {
                list = Conexao.ConsultaBanco<List<Deposito>>(SQL.ToString(), true);
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return list;
        }
        #endregion

        #region Search
        public List<Deposito> Search(int filial, string deposito)
        {
            List<Deposito> list = new List<Deposito>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine($"SELECT \"WhsCode\", \"WhsName\" FROM {ConstantUtils.QUERYBASE}\"OWHS\" WHERE \"BPLid\" =" + filial + " ");
            SQL.AppendLine("AND (( UPPER(\"WhsName\") like '%" + deposito + "%')OR( UPPER(\"WhsCode\") like '%" + deposito + "%'))");
            SQL.AppendLine("ORDER BY \"WhsName\" ASC ");

            return list;
        }
        #endregion

        #region Retorna depósito de filial padrao
        //public string WhsDefault(int CodFilial)
        //{
        //    string WhsCode = "";

        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;

        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //    StringBuilder SQL = new StringBuilder();

        //    SQL.AppendLine("SELECT \"DflWhs\" FROM OBPL WHERE \"BPLId\" = '" + CodFilial + "'");

        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    if (!oRecordset.EoF)
        //    {
        //        WhsCode = oRecordset.Fields.Item("DflWhs").Value.ToString();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return WhsCode;
        //}
        #endregion
    }
}