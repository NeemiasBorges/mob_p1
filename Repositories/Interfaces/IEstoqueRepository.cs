﻿using Model.Estoque;
using Models;
using Models.Estoque;
using PortalMOB.Models;
using PowerOne.Models;
using System;
using System.Collections.Generic;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IEstoqueRepository
    {
        int contEstoque();
        ItemEstoque GetTopRequestedItemInMonth(DateTime date);
        List<RequerimentoEstoque> List(Login login, string dtInicio, string dtFim, string status, string tipo);
        RequerimentoEstoque GetByKey(int DocEntry);
        List<RequerimentoEstoqueLinhas> ListarLinhas(int DocEntry, string[] linhas);
        List<Projetos> getProjeto();
        Resultado AddReq(RequerimentoEstoque requerimento, Login login);
       string EditAdd(RequerimentoEstoqueLinhas linha, int tipo);
    }
}