﻿using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IAutorizacoesRepository
    {
        List<Login> GetAllIdUsers();
        string CriarAutorizacoes();
        string CriarAutorizacoes(Login user);
        Autorizacoes PegarAutorizacoes(string code);
        AutorizacoesGrupo PegarGrupoAutorizacoes(string code);
        List<Autorizacoes> List();
        List<AutorizacoesGrupo> ListGrupo();
        string Update(Autorizacoes autorizacao);
        string AtualizarAutorizacoesGrupos(AutorizacoesGrupo autorizacao);
        string AtualizarGrupoDoUsuario(string idUser, string idGrupo);
    }
}