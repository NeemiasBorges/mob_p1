﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IItemEstoqueRepository
    {
        List<ItemEstoque> List();
        List<ItemEstoque> Search(string name);
    }
}

