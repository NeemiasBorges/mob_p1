﻿using Models.Estoque;
using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IAlmoxarifadoRepository
    {
        List<RequerimentoEstoque>List(
              Login login, string dtInicio, string dtFim, string status, int tipo
           );

        string AttReq(int DocEntry,string Status);

        string FecharRequerimento(RequerimentoEstoque requerimento,Login login,string Status);

    }
}
