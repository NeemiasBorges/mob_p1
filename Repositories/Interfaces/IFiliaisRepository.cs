﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IFiliaisRepository
    {
        List<Filiais> List();
    }
}
