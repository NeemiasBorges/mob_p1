﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IDepositoRepository
    {
        List<Deposito> List(int Filial);
        List<Deposito> Search(int Filial, string deposito);

    }
}
