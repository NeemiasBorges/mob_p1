﻿using Models.Estoque;
using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IAprovacaoRepository
    {
        List<RequerimentoEstoque> searchList(string DataInicio,string  DataFim,string  Status);

        string aprovaReq(string DocEntry, Login login);

        string fecharReq(string DocEntry);

    }
}
