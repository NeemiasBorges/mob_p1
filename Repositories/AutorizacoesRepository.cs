﻿using DAL;
using DataBase;
using Newtonsoft.Json;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Utils;

namespace PowerOne.Repository
{
    public class AutorizacoesRepository : IAutorizacoesRepository
    {
        #region Pegar Id Todos os Usuários 
        public List<Login> GetAllIdUsers()
        {
            List<Login> usersId = new List<Login>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT \"empID\", \"firstName\" FROM \"OHEM\"");

            try
            {
                usersId = Conexao.ConsultaBanco<List<Login>>(SQL.ToString(), true);
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, Log.TipoLog.Erro);
            }

            return usersId;
        }
        #endregion

        #region Criar Autorizações para Todos os Usuários
        public string CriarAutorizacoes()
        {
            string response = null;

            List<Login> users = GetAllIdUsers();

            try
            {
                foreach (Login user in users)
                {
                    string url = $"{ConstantUtils.URL_SERVICE_LAYER}U_P1_ACCESS";


                    string Code = user.Id.ToString();
                    string Name = user.User;
                    string U_P1_EMPId = user.Id.ToString();

                    var json = new { Code, Name, U_P1_EMPId };

                    response = RequestUtils.Request(url, RequestUtils.POST, LoginDAL.sessionId, json.ToString(), false);
                }
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }
        #endregion

        #region Criar Autorizacao 
        public string CriarAutorizacoes(Login user)
        {
            string response = null;

            try
            {
                string url = $"{ConstantUtils.URL_SERVICE_LAYER}U_P1_ACCESS";

                string Code = user.Id.ToString();
                string Name = user.User;
                string U_P1_EMPId = user.Id.ToString();

                var json = new { Code, Name, U_P1_EMPId };

                response = RequestUtils.Request(url, RequestUtils.POST, LoginDAL.sessionId, json.ToString(), false);
            }
            catch (Exception e)
            {
                response = e.Message;

                Log.Gravar(e.Message, Log.TipoLog.Erro);
            }

            return response;
        }
        #endregion

        #region Pegar Autorizações do Usuário
        public Autorizacoes PegarAutorizacoes(string code)
        {
            Autorizacoes autorizacao = new Autorizacoes();

            try
            {
                StringBuilder SQL = new StringBuilder();

                SQL.AppendLine("SELECT ");
                SQL.AppendLine("\"U_P1_ReqS\" AS \"ReqEstoque\", ");
                SQL.AppendLine("\"U_P1_NewReqS\" AS \"CriarReqEstoque\", ");
                SQL.AppendLine("COALESCE(\"U_P1_AUTORIZ\",'N') AS \"Autorizador\" ,COALESCE(\"U_P1_ALMOX\",'N') AS \"Almoxarifado\", ");
                SQL.AppendLine("\"U_P1_Grupo\" AS \"idGrupo\" ");
                SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"@P1_ACCESS\" ");
                SQL.AppendLine("WHERE \"Code\" = '" + code + "' FOR JSON PATH");

                autorizacao = Conexao.ConsultaBanco<Autorizacoes[]>(SQL.ToString(),false)[0];
            }
            catch (Exception)
            {
                autorizacao = JsonConvert.DeserializeObject<Autorizacoes>("{\"ReqEstoque\": 'R',\"CriarReqEstoque\": 'R',\"Coordenador\" : 'N',\"IdGrupo\" : '0'}");
            }

            return autorizacao;
        }
        #endregion

        #region Pegar Autorizações
        public List<Autorizacoes> List()
        {

            List<Autorizacoes> autorizacoes = new List<Autorizacoes>();

            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine($"SELECT T0.\"U_P1_ReqS\" AS \"ReqEstoque\", T0.\"U_P1_NewReqS\" AS \"CriarReqEstoque\", ");
                SQL.AppendLine($"T0.\"U_P1_AUTORIZ\" AS \"Autorizador\", T0.\"U_P1_ALMOX\" AS \"Almoxarifado\", ");
                SQL.AppendLine($"T0.\"U_P1_Grupo\" AS \"IdGrupo\", T0.\"U_P1_Type\" AS \"Tipo\", T0.\"U_P1_Status\",");
                SQL.AppendLine($"H.\"empID\" AS \"UserId\", H.\"firstName\" AS \"UserName\", H.\"lastName\" AS \"UserLastName\"");
                SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"@P1_ACCESS\" T0 INNER JOIN {ConstantUtils.QUERYBASE}OHEM H ");
                SQL.AppendLine("ON T0.\"U_P1_EMPId\" = H.\"empID\" ");
                SQL.AppendLine("ORDER BY CAST(\"U_P1_EMPId\" as NUMERIC)");

                autorizacoes = Conexao.ConsultaBanco<List<Autorizacoes>>(SQL.ToString(),true);
            }
            catch (Exception)
            {
                
            }
          
            return autorizacoes;
        }
        #endregion

        #region Pegar Autorizações do Grupo
        public AutorizacoesGrupo PegarGrupoAutorizacoes(string code)
        {
            AutorizacoesGrupo autorizacoes = new AutorizacoesGrupo();

            try
            {
                StringBuilder SQL = new StringBuilder();

                SQL.AppendLine("SELECT  \"Code\",  \"Name\", ");
                SQL.AppendLine("\"U_P1_ReqS\" AS \"ReqEstoque\", ");
                SQL.AppendLine("\"U_P1_NewReqS\" AS \"CriarReqEstoque\",");
                SQL.AppendLine("COALESCE(\"U_P1_AUTORIZ\", 'N') AS \"Autorizador\", COALESCE(\"U_P1_ALMOX\", 'N') AS \"Almoxarifado\"");
                SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"@P1_ACCGRP\" ");
                SQL.AppendLine("WHERE \"Code\" = '" + code + "' FOR JSON PATH");

                autorizacoes = Conexao.ConsultaBanco<AutorizacoesGrupo[]>(SQL.ToString())[0];
            }
            catch (NullReferenceException)
            {
                autorizacoes = null;
            }
            catch (Exception e)
            {
                autorizacoes = JsonConvert.DeserializeObject<AutorizacoesGrupo>("{\"ReqEstoque\": 'R',\"CriarReqEstoque\": 'R',\"Coordenador\" : 'N',\"IdGrupo\" : '0'}");
                Log.Gravar(e.Message, Log.TipoLog.Erro);
            }

            return autorizacoes;
        }
        #endregion


        #region Atualizar Autorizações
        public string Update(Autorizacoes autorizacao)
        {
            string response = null;

            try
            {               
                string url = ConstantUtils.URL_SERVICE_LAYER + $"U_P1_ACCESS('{autorizacao.UserId}')";
                string json = "{\"U_P1_ReqS\":\"" + autorizacao.ReqEstoque + "\", \"U_P1_NewReqS\":\"" + autorizacao.CriarReqEstoque + "\", \"U_P1_AUTORIZ\":\"" + autorizacao.Autorizador + "\", \"U_P1_ALMOX\":\"" + autorizacao.Almoxarifado + "\"}";

                response = RequestUtils.Request(url, RequestUtils.PATCH, LoginDAL.sessionId, json, false);

                response = response == "Atualizado com sucesso!" ? null : response;
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }
        #endregion

        #region Atualizar Autorizações de Grupos
        public string AtualizarAutorizacoesGrupos(AutorizacoesGrupo autorizacoes)
        {
            string msg = null;

            try
            {
                string url = ConstantUtils.URL_SERVICE_LAYER + $"U_P1_ACCGRP('{autorizacoes.Code}')";
                string json = "{\"U_P1_ReqS\":\"" + autorizacoes.ReqEstoque + "\", \"U_P1_NewReqS\":\"" + autorizacoes.CriarReqEstoque + "\", \"U_P1_AUTORIZ\":\"" + autorizacoes.Autorizador + "\", \"U_P1_ALMOX\":\"" + autorizacoes.Almoxarifado + "\"}";

                msg = RequestUtils.Request(url, RequestUtils.PATCH, LoginDAL.sessionId, json, false);

                msg = msg == "Atualizado com sucesso!" ? null : msg;
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return msg;
        }
        #endregion

        #region Atualizar Grupo de Usuário
        public string AtualizarGrupoDoUsuario(string idUser, string idGrupo)
        {
            string msg = null;

            try
            {

                string url = ConstantUtils.URL_SERVICE_LAYER + $"U_P1_ACCESS('{idUser}')";
                string json = "{\"U_P1_Grupo\":\"" + idGrupo + "\"}";

                msg = RequestUtils.Request(url, RequestUtils.PATCH, LoginDAL.sessionId, json, false);
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return msg;
        }
        #endregion

        //#region Pesquisar Autorizações do Cliente
        //public Autorizacoes PegarAutorizacoesPN(string code)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT COALESCE(T0.\"U_P1_CodGrupo\",'') \"U_P1_CodGrupo\", COALESCE(T1.\"U_P1_RECR\",'N') \"U_P1_RECR\", COALESCE(T1.\"U_P1_ATTDADOS\",'N') \"U_P1_ATTDADOS\", COALESCE(T1.\"U_P1_CNTRVIS\",'N') \"U_P1_CNTRVIS\", COALESCE(T1.\"U_P1_EMAILSEND\",'N') \"U_P1_EMAILSEND\", COALESCE(T1.\"U_P1_FTRVIEW\",'N') \"U_P1_FTRVIEW\" FROM \"OCPR\" T0");
        //    SQL.AppendLine("INNER JOIN \"@P1_GRPCNTC\" T1 ON T1.\"Code\" = T0.\"U_P1_CodGrupo\" WHERE T0.\"CntctCode\" = '" + code + "'");

        //    Autorizacoes autorizacao = new Autorizacoes();

        //    oRecordSet.DoQuery(SQL.ToString());
        //    oRecordSet.MoveFirst();
        //    if (!oRecordSet.EoF)
        //    {
        //        autorizacao.RecebeEmail          = oRecordSet.Fields.Item("U_P1_RECR").Value.ToString();
        //        autorizacao.AttDados             = oRecordSet.Fields.Item("U_P1_ATTDADOS").Value.ToString();
        //        autorizacao.Questiona            = oRecordSet.Fields.Item("U_P1_EMAILSEND").Value.ToString();
        //        autorizacao.VisualizarFatura     = oRecordSet.Fields.Item("U_P1_FTRVIEW").Value.ToString();
        //        autorizacao.ExibeContrato        = oRecordSet.Fields.Item("U_P1_CNTRVIS").Value.ToString();
        //    }
        //    else
        //    {
        //        autorizacao.RecebeEmail          = "N";
        //        autorizacao.AttDados             = "N";
        //        autorizacao.Questiona            = "N";
        //        autorizacao.VisualizarFatura     = "N";
        //        autorizacao.ExibeContrato        = "N";
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return autorizacao;
        //}
        //#endregion

        //#region Atualizar Definições de Bloqueio
        //public string SetLock(string StatusBloqueio, string IsHour, DateTime HourLock, int DaysOfLock)
        //{
        //    string msg = null;
        //    int iRetcode;
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oConfDoc = oCompany.UserTables.Item("P1_CONFDC");

        //    try
        //    {
        //        bool bExiste = false;
        //        bExiste = oConfDoc.GetByKey("1");

        //        oConfDoc.UserFields.Fields.Item("U_P1_STBLOCK").Value = String.IsNullOrEmpty(StatusBloqueio) ?"N" :StatusBloqueio;
        //        oConfDoc.UserFields.Fields.Item("U_P1_ISHOUR").Value = String.IsNullOrEmpty(IsHour) ?"N" : IsHour;
        //        oConfDoc.UserFields.Fields.Item("U_P1_HOURLOCK").Value = HourLock;
        //        oConfDoc.UserFields.Fields.Item("U_P1_DAYSLOCK").Value = DaysOfLock;

        //        if (bExiste)
        //        {
        //            iRetcode = oConfDoc.Update();
        //        }
        //        else
        //        {
        //            oConfDoc.Code = "1";
        //            oConfDoc.Name = "Financeiro";

        //            iRetcode = oConfDoc.Add();
        //        }

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfDoc);
        //        oConfDoc = null;
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Pegar Definições de Bloqueio
        //public ConfDocumento PegarConfBloqueio()
        //{
        //    ConfDocumento configuracoesBloqueio = new ConfDocumento();

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"U_P1_STBLOCK\", \"U_P1_ISHOUR\", COALESCE(\"U_P1_DAYSLOCK\",'0') AS \"U_P1_DAYSLOCK\", \"U_P1_HOURLOCK\" FROM \"@P1_CONFDC\" ");
        //    SQL.AppendLine("WHERE \"Code\" = '1' ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    if (!oRecordset.EoF)
        //    {
        //        configuracoesBloqueio.StatusBloqueio     = oRecordset.Fields.Item("U_P1_STBLOCK").Value.ToString();
        //        configuracoesBloqueio.IsHourBloqueio     = oRecordset.Fields.Item("U_P1_ISHOUR").Value.ToString();
        //        configuracoesBloqueio.HorasBloqueio      = TrataHora.HorasViaString(oRecordset.Fields.Item("U_P1_HOURLOCK").Value.ToString());
        //        configuracoesBloqueio.BeforeDaysBloqueio = int.Parse(oRecordset.Fields.Item("U_P1_DAYSLOCK").Value.ToString());
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return configuracoesBloqueio;
        //}
        //#endregion


        //#region Atualizar Permissões de lançamento  em datas anteriores
        //public string AtualizarPermLancamento(string UserID, DateTime LimitDate, string YNLimitDate)
        //{
        //    string msg = null;

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_ACCESS");

        //    if (oAutorizacoes.GetByKey(UserID))
        //    {
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_YNLIMITDT").Value = YNLimitDate;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_LIMITDATE").Value = LimitDate;

        //        if (oAutorizacoes.Update() != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //        }
        //    }
        //    else
        //    {
        //        msg = "Usuário não encontrado";
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAutorizacoes);

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Pegar Permiçãoo de lançamento por usuário
        //public Autorizacoes GetPermLancamentoByUser(string UserID)
        //{
        //    Autorizacoes permicao = new Autorizacoes();

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"U_P1_YNLIMITDT\", \"U_P1_LIMITDATE\" FROM \"@P1_ACCESS\"");
        //    SQL.AppendLine("WHERE \"Code\" = '" + UserID + "'");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    oRecordSet.MoveFirst();
        //    if (!oRecordSet.EoF)
        //    {
        //        permicao.YNLimitDate = oRecordSet.Fields.Item("U_P1_YNLIMITDT").Value.ToString();
        //        permicao.LimitDate = DateTime.Parse(oRecordSet.Fields.Item("U_P1_LIMITDATE").Value.ToString());
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;


        //    return permicao;
        //}
        //#endregion

        #region Pegar Autorizações do Grupo
        public List<AutorizacoesGrupo> ListGrupo()
        {

            List<AutorizacoesGrupo> autorizacoesGrupo = new List<AutorizacoesGrupo>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT  \"Code\",  \"Name\", ");
            SQL.AppendLine("\"U_P1_ReqS\" AS \"ReqEstoque\", ");
            SQL.AppendLine("\"U_P1_NewReqS\" AS \"CriarReqEstoque\",");
            SQL.AppendLine("COALESCE(\"U_P1_AUTORIZ\", 'N') AS \"Autorizador\", COALESCE(\"U_P1_ALMOX\", 'N') AS \"Almoxarifado\"");
            SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"@P1_ACCGRP\" ");

            try
            {
                autorizacoesGrupo = Conexao.ConsultaBanco<List<AutorizacoesGrupo>>(SQL.ToString(),true);
            }
            catch (Exception)
            {

            }

            return autorizacoesGrupo;
        }
        #endregion
    }
}