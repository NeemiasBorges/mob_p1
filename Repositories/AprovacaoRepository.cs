﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using PowerOne.Controllers;
using System.Threading;
using System.Globalization;
using PortalMOB.Repositories.Interfaces;
using Models.Estoque;
using Utils;
using DataBase;
using static PowerOne.Util.Log;
using DAL;
using Newtonsoft.Json;
using PortalMOB.Repositories;

namespace PowerOne.Repository
{
    public class AprovacaoRepository : IAprovacaoRepository
    {

        #region funcoes
        EnvioEmail envio = new EnvioEmail();
        EstoqueRepository estoque = new EstoqueRepository();
        #endregion

        #region aprovacao interna
        //public string caiNaAprovacao(string tipoDoc, int DocEntry, string Etapa, Login login)
        //{
        //    Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
        //    int iRetcode;
        //    string msg = null;
        //    try
        //    {
        //        SAPbobsCOM.Company oCompany;
        //        oCompany = Conexao.Company;
        //        SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_APRVDOC");
        //        if (oAccess.GetByKey(DocEntry.ToString()))
        //        {
        //            //oAccess.UserFields.Fields.Item("U_P1_CLASSDOC").Value = tipoDoc;
        //            oAccess.UserFields.Fields.Item("U_P1_NVLAPRV").Value = Etapa;
        //            //oAccess.UserFields.Fields.Item("U_P1_DOCNUM").Value = DocEntry;

        //            iRetcode = oAccess.Update();
        //        }
        //        else
        //        {
        //            oAccess.Name = DocEntry.ToString();
        //            oAccess.Code = DocEntry.ToString();
        //            oAccess.UserFields.Fields.Item("U_P1_CLASSDOC").Value = tipoDoc;
        //            oAccess.UserFields.Fields.Item("U_P1_NVLAPRV").Value = Etapa;
        //            oAccess.UserFields.Fields.Item("U_P1_DOCNUM").Value = DocEntry;
        //            oAccess.UserFields.Fields.Item("U_P1_DTSOLIC").Value = DateTime.UtcNow;

        //            iRetcode = oAccess.Add();
        //        }


        //        if (iRetcode != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //        oAccess = null;

        //        if (msg != null)
        //        {
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    return msg;
        //}
        #endregion

        #region pega Aprovacoes
        public List<RequerimentoEstoque> searchList(string DataInicio, string DataFim, string Status)
        {
            List<RequerimentoEstoque> lista = new List<RequerimentoEstoque>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", COALESCE(T0.\"U_P1_IDFILIAL\", 0) \"U_P1_IDFILIAL\", ");
            SQL.AppendLine("CASE WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'O'  THEN 'Aberta' WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'A' THEN 'Aprovada' ELSE 'Cancelada' END \"U_P1_STATUS\", ");
            SQL.AppendLine("COALESCE(T0.\"U_P1_DTLANC\",'01/01/2021') \"U_P1_DTLANC\", ");
            SQL.AppendLine("CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"U_P1_Owner\" ");
            SQL.AppendLine($"FROM {ConstantUtils.QUERYBASE}\"@P1_OIGE\" T0 INNER JOIN {ConstantUtils.QUERYBASE}OHEM H ");
            SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");

            #region Filtros
            if (!String.IsNullOrEmpty(DataInicio))
                SQL.AppendLine("AND T0.\"U_P1_DTLANC\" >= '" + (DateTime.ParseExact(DataInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' ");

            if (!String.IsNullOrEmpty(DataFim))
                SQL.AppendLine("AND T0.\"U_P1_DTLANC\" <='" + (DateTime.ParseExact(DataFim, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyyMMdd") + "' ");

            if (!String.IsNullOrEmpty(Status))
                SQL.AppendLine("AND T0.\"U_P1_STATUS\" = '" + Status + "' ");

            #endregion

            SQL.AppendLine("AND T0.\"U_P1_STATUS\" = 'O'");
            SQL.AppendLine("ORDER BY T0.\"DocEntry\" DESC ");
            //SQL.AppendLine("FOR JSON PATH ");

            try
            {
                lista = Conexao.ConsultaBanco<List<RequerimentoEstoque>>(SQL.ToString(), true);
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return lista;
        }


        #endregion

        #region aprovar

        public string aprovaReq(string DocEntry, Login user)
        {
            string resul = null;
            string urlHeader = ConstantUtils.URL_SERVICE_LAYER + "P1_OIGE(" + DocEntry + ")";
            string Header = "{ \"U_P1_STATUS\": \"A\", \"DocEntry\" :\"" + DocEntry + "\",\"U_P1_APROVADOR\":\""+user.User+"\" }";

            try
            {
                resul = RequestUtils.Request(urlHeader, RequestUtils.PATCH, LoginDAL.sessionId, Header, false);
                List<Login> usuarios = new List<Login>();
                usuarios = getalmoxarifado();
                usuarios.Add(user);
                NotificaUsuario(usuarios, int.Parse(DocEntry));
            }
            catch (Exception)
            {

            }

            return resul;
        }

        #endregion

        #region Notifica soliciante estoque

        public void NotificaUsuario(List<Login> users, int DocEntry)
        {
            RequerimentoEstoque estReq = estoque.GetByKey(DocEntry);

            if (!string.IsNullOrEmpty(estReq.U_P1_Email))
            {
                foreach (var user in users)
                {
                    List<String> itens = new List<String>();
                    foreach (var item in estReq.P1_IGE1Collection)
                    {
                        itens.Add("<tr style=\"text-align:center;\"><td>" + item.U_P1_Quantity + " </td><td> " + item.U_P1_DescItem + " </td></tr>");
                    }

                    envio.EnviaEmail(user.email, 2, user.User.Replace(".", " "), DocEntry.ToString(), itens);
                }

            }
        }

        #endregion

        #region pega responsavel pelo setor estoque

        public List<Login> getalmoxarifado()
        {
            List<Login> respSetor = new List<Login>();
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT concat(concat(T0.\"firstName\", '.'), T0.\"lastName\") AS \"User\",");
            SQL.AppendLine("coalesce(T0.\"email\", '') \"email\"");
            SQL.AppendLine("FROM OHEM T0");
            SQL.AppendLine($"INNER JOIN {ConstantUtils.QUERYBASE}\"@P1_ACCESS\" T1 ON T1.\"U_P1_EMPId\" = T0.\"empID\"");
            SQL.AppendLine("WHERE T1.\"U_P1_ALMOX\" = 'T'");

            try
            {
                respSetor = Conexao.ConsultaBanco<List<Login>>(SQL.ToString(), true);
            }
            catch (Exception)
            {
                throw;
            }

            return respSetor;
        }

        #endregion

        #region fechar

        public string fecharReq(String DocEntry)
        {
            string result = null;
            string urlHeader = ConstantUtils.URL_SERVICE_LAYER + "P1_OIGE(" + DocEntry + ")";
            string Json = "{ \"U_P1_STATUS\": \"C\", \"DocEntry\":\"" + DocEntry + "\"}";

            try
            {
                result = RequestUtils.Request(urlHeader, RequestUtils.PATCH, LoginDAL.sessionId, Json, false);
            }
            catch (Exception)
            {

            }

            return result;
        }

        #endregion
    }
}