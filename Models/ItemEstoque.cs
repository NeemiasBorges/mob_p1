﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public class ItemEstoque
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string TipoItem { get; set; }

        [Display(Name = "Grupo Item")]
        public string GroupItem { get; set; }
        public string ItemClass { get; set; }
        public string ItmsGrpNam { get; set; }
        public string InvntryUom { get; set; }
        public string UnidadeMedida { get; set; }
        public float EmEstoque { get; set; }
        public float Confirmado { get; set; }
        public float Pedido { get; set; }
        public float Disponivel { get; set; }
    }
}

