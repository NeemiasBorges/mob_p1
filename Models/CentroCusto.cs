﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class CentroCusto
    {
        public string OcrCode { get; set; }
        public string OcrName { get; set; }
        public int DimCode { get; set; }
        public string PrcCode { get; set; }
        public string PrcName { get; set; }     

    }
}