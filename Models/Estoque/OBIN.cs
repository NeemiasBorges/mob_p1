﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Models.Estoque
{
    public class OBIN
    {
        public string AbsEntry { get; set; }
        public string WhsCode { get; set; }
        public string SL1Abs { get; set; }
        public string BinCode { get; set; }
        public string Descr { get; set; }
        public string SL1code { get; set; }
    }
}