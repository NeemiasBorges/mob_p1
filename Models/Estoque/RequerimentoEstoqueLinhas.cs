﻿using Models.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model.Estoque
{
    public class RequerimentoEstoqueLinhas
    {
        #region Lista de propriedades

        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº Linha")]
        public int LineId { get; set; }

        [Display(Name = "Status Linha")]
        public string U_P1_StatusLinha { get; set; }

        [Display(Name = "Código")]
        public string U_P1_ItemCode { get; set; }
        public string U_P1_DescItem { get; set; }

        //[Display(Name = "Grupo Item")]
        //public string GroupItem { get; set; }

        //[Display(Name = "Quantidade Disponível em Estoque")]
        //public double GetQtdDisp { get; set; }

        //[Display(Name = "Descrição Lote")]
        //public string BatchNumber { get; set; }

        //[Display(Name = "Controle Lote")]
        //public string ManBtchNum { get; set; }

        //[Display(Name = "Unidade de Medida")]
        //public string UndMedida { get; set; }

        [Display(Name = "Dt. Requisição")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public string U_P1_DTEFET { get; set; }

        [Display(Name = "Descrição")]
        public string U_P1_Comments { get; set; }

        [Display(Name = "Código Depósito")]
        public string U_P1_WhsCode { get; set; }

        //[Display(Name = "Descrição Depósito")]
        //public string DescricaoDeposito { get; set; }

        [Display(Name = "Quantidade")]
        public string U_P1_Quantity { get; set; }

        public string U_P1_GroupItem { get; set; }
        public string U_P1_UndMedida { get; set; }
        public string U_P1_PROJ { get; set; }
        public string U_P1_WhsName { get; set; }
        public string U_P1_ITMCT { get; set; }
        [DefaultValue("")]

        [Display(Name = "Centro de Custo")]
        public string U_P1_CC01 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Nome Ct. Custo")]
        public string U_P1_OcrName { get; set; }
        [DefaultValue("")]

        [Display(Name = "Centro de Custo2")]
        public string U_P1_CC02 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Nome Ct. Custo2")]
        public string U_P1_OcrName2 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Centro de Custo3")]
        public string U_P1_CC03 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Nome Ct. Custo3")]
        public string U_P1_OcrName3 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Centro de Custo4")]
        public string U_P1_CC04 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Nome Ct. Custo4")]
        public string U_P1_OcrName4 { get; set; }

        [Display(Name = "Centro de Custo5")]
        [DefaultValue("")]
        public string U_P1_CC05 { get; set; }
        [DefaultValue("")]

        [Display(Name = "Nome Ct. Custo5")]
        public string U_P1_OcrName5 { get; set; }


        //public List<Lotes> Lotes { get; set; }


        //[Display(Name = "Nº Requisição")]
        //public int DocNum { get; set; }

        //[Display(Name = "Status")]
        //public string U_P1_STATUS { get; set; }

        //[Display(Name = "Observação")]
        //public string Observacao { get; set; }

        #endregion
    }
}