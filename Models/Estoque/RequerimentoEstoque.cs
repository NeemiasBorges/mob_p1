﻿using Model.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models.Estoque
{
    public class RequerimentoEstoque
    {
        public int DocEntry { get; set; }

        [Display(Name = "Nº do Documento")]
        public int DocNum { get; set; }

        [Display(Name = "Status")]
        public string U_P1_STATUS { get; set; }         


        [Display(Name = "Dt. Lançamento")]
        public string U_P1_DTLANC { get; set; }                     

        [Display(Name = "Requisitante")]
        public string U_P1_Owner { get; set; }

        [Display(Name = "Observação")]
        public string U_P1_Comments { get; set; }
        public string Canceled { get; set; }

        [Display(Name = "Código Filial")]
        public int U_P1_IDFILIAL { get; set; }

        [Display(Name = "Nome da Filial")]
        public string U_P1_NOMEFILIAL { get; set; }

        public string U_P1_Email { get; set; }
        public string Creator { get; set; }
        public string U_P1_APROVADOR { get; set; }

        public List<RequerimentoEstoqueLinhas> P1_IGE1Collection { get; set; }

    }
}
