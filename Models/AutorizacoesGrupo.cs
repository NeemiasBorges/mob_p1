﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class AutorizacoesGrupo
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ReqEstoque { get; set; }
        public string CriarReqEstoque { get; set; }
        public string Autorizador { get; set; }
        public string Almoxarifado { get; set; }

    }
}