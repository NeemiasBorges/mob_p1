﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class Autorizacoes
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserLastName { get; set; }
        public string ReqEstoque { get; set; }
        public string CriarReqEstoque { get; set; }
        public string IdGrupo { get; set; }
        public AutorizacoesGrupo Grupo { get; set; }
        public string Tipo { get; set; }
        public string Autorizador { get; set; }
        public string Almoxarifado { get; set; }
    }

   
}