﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Models
{
    public class Resultado
    {
        public string Msg { get; set; }
        public string Tipo { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string Erro { get; set; }
        public string data { get; set; }
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
    }
}