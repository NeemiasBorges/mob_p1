﻿//inicio input filial
function SearchFilial() {
    var BPLName = $('#pesquisar-filial').val();
    var BPLName = BPLName.toUpperCase()

    $.ajax({
        url: '/BP/SearchFilial',
        type: 'get',
        dataType: 'json',
        data: {
            'BPLName': BPLName,
        },
        success: function (data) {
            listarFilial(data);
        },
    });
}

function listarFilial(listItens) {
    $('tbody#lista-tabela-filial tr').remove();
    listItens.forEach(function (filial) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + filial.BplId + "' onchange='preencherFilial(this);listarDepositos()' value='" + filial.BplName + "' data-bplid='" + filial.BplId + "'></td>";
        row += "<td>" + filial.BplId + "</td>";
        row += "<td>" + filial.BplName + "</td>";

        $('tbody#lista-tabela-filial').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='filial' data-id='" + filial.BplId + "'>" + row + "<tr>");
    });
}

function preencherFilial(_radio) {
    $('#pesquisar-filial').val(jQuery(_radio).val());
    $('#idFilial').val(jQuery(_radio).data("bplid"));
    $('#filial-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-filial', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchFilial();
        $('#filial-modal').modal('toggle');
    }
});
//fim input filial

// listar depósitos com select
function listarDepositos() {
    if ($('#idFilial') == "null") {
        $('select#deposito option').remove();
    } else {
        $.ajax({
            url: '/Deposito/ListDepositos',
            type: 'get',
            dataType: 'json',
            data: {
                'Filial': $('#idFilial').val()
            },
            success: function (data) {
                preenhceSelectDeposito(data)
            },
            error: function () {
                alert("Erro ao enviar os dados.");
            }
        });
    }
}

function preenhceSelectDeposito(data) {
    $('select#deposito option').remove();

    $('select#deposito').append("<option value=''></option");

    var DefaultDeposito = $('#DefaultDeposito').val()
    var DefaultDepositoId = $('#DefaultDeposito').attr('data-id')

    //console.log(DefaultDeposito)
    //console.log(DefaultDepositoId)

    data.forEach(function (deposito) {
        var row = '';

        if (DefaultDepositoId == deposito.WhsCode) {
            row += "<option value='" + deposito.WhsCode + "' selected>" + deposito.WhsCode + " - " + deposito.WhsName + "</option>";
        } else {
            row += "<option value='" + deposito.WhsCode + "'>" + deposito.WhsCode + " - " + deposito.WhsName + "</option>";
        }

        $('select#deposito').append(row);
    });
}
// fim select depósito


//inicio input bp
function listarBp() {
    var CardName = $('#pesquisar-fornecedores').val();
    var CardName = CardName.toUpperCase()
    var Filial = $('#idFilial').val();

    $.ajax({
        url: '/BP/SearchBp',
        type: 'get',
        dataType: 'json',
        data: {
            'CardName': CardName,
            'Filial': Filial,
        },
        success: function (data) {
            listarBpRadio(data);
        },
    });
}

function listarBpRadio(listItens) {
    $('tbody#lista-tabela-bp tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.CardCode + "' onchange='escreverBpInput(this)' ' value='" + item.CardName + "' data-cardcode='" + item.CardCode + "'></td>";
        row += "<td>" + item.CardCode + "</td>";
        row += "<td>" + item.CardName + "</td>";

        $('tbody#lista-tabela-bp').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='bp' data-id='" + item.CardCode + "'>" + row + "<tr>");
    });
}

function escreverBpInput(_radio) {
    $('#pesquisar-fornecedores').val(jQuery(_radio).val());
    $('#idFonecdor').val(jQuery(_radio).data("cardcode"));
    $('#bp-modal').modal('toggle');
}

$(document).on('keydown', '#pesquisar-fornecedores', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        listarBp();
        $('#bp-modal').modal('toggle');
    }
});
//fim input bp

// listar itens Financeiros

// listar itens input
function listarItens() {

    var itemName = $('#pesquisar-item').val();
    var itemName = itemName.toUpperCase()

    $.ajax({
        url: '/Item/Search',
        type: 'get',
        dataType: 'json',
        data: {
            'itemName': itemName,
        },
        success: function (data) {
            listarItensRadio(data);
        }
    });
}

function listarItensRadio(listItens) {
    $('tbody#lista-tabela-itens tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.ItemCode + "' onchange='escreverItemInput(this)' value='" + item.ItemCode + "' data-itemname='" + item.ItemName + "' data-unidademedida='" + item.UnidadeMedida + "' data-groupitem='" + item.GroupItem + "'>" + "</td>";
        row += "<td>" + item.ItemCode + "</td>";
        row += "<td>" + item.ItemName + "</td>";
        row += "<td>" + item.ItemClass + "</td>";

        $('tbody#lista-tabela-itens').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='item' data-id='" + item.ItemCode + "'>" + row + "<tr>");
    });
}

function escreverItemInput(_radio) {
    $('#desc-item').val(jQuery(_radio).val());
    $('#pesquisar-item').val(jQuery(_radio).data("itemname"));
    $('#group-item').val(jQuery(_radio).data("groupitem"));

    $('#unidadeMedida').val(jQuery(_radio).data("unidademedida"));

    $('#itens-modal').modal('hide');
}

        // faz o modal de itens aparecer quando se aperta tab e enter
$(document).on('keydown', '#pesquisar-item', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        listarItens();
        $('#itens-modal').modal('toggle');
    }
});
// fim listar itens input


//inicio input projeto
function SearchProjeto() {
    var pesquisa = $('#pesquisar-projeto').val();
    var pesquisa = pesquisa.toUpperCase()

    $.ajax({
        url: '/Searchs/Projeto',
        type: 'get',
        dataType: 'json',
        data: {
            'pesquisa': pesquisa,
        },
        success: function (data) {
            listarProjeto(data);
        },
    });
}

function listarProjeto(listItens) {
    $('tbody#lista-tabela-projeto tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.PrjCode + "' onchange='preencherProjeto(this);' value='" + item.PrjName + "' data-id='" + item.PrjCode + "'></td>";
        row += "<td>" + item.PrjCode + "</td>";
        row += "<td>" + item.PrjName + "</td>";

        $('tbody#lista-tabela-projeto').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='projeto' data-id='" + item.PrjCode + "'>" + row + "<tr>");
    });
}

function preencherProjeto(_radio) {
    $('#pesquisar-projeto').val(jQuery(_radio).data("id") + " - " + jQuery(_radio).val());
    $('#idProjeto').val(jQuery(_radio).data("id"));
    $('#projeto-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-projeto', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchProjeto();
        $('#projeto-modal').modal('toggle');
    }
});
//fim input projeto

//inicio input projeto Detail
function SearchProjetoDetail(elemento) {

    let linhaTabela = jQuery(elemento).parent().parent();
    let pesq = jQuery(linhaTabela).find('.pesquisar-projetoDt').val();
    //console.log(linhaTabela)
    //console.log(pesq)

    var pesquisa = pesq;
    var pesquisa = pesquisa.toUpperCase()

    $.ajax({
        url: '/Searchs/Projeto',
        type: 'get',
        dataType: 'json',
        data: {
            'pesquisa': pesquisa,
        },
        success: function (data) {
            listarProjetoDt(data);
        },
    });
}

function listarProjetoDt(listItens) {
    $('tbody#lista-tabela-projetoDt tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.PrjCode + "' onclick='fnc(this)' data-tipo='projetoDt' value='" + item.PrjName + "' data-id='" + item.PrjCode + "'></td>";
        row += "<td>" + item.PrjCode + "</td>";
        row += "<td>" + item.PrjName + "</td>";

        $('tbody#lista-tabela-projetoDt').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='projetoDt' data-id='" + item.PrjCode + "'>" + row + "<tr>");
    });
}

function preencherProjetoDt(_radio) {
    $('.pesquisar-projetoDt').val(jQuery(_radio).data("id") + " - " + jQuery(_radio).val());
    $('.idProjeto').val(jQuery(_radio).data("id"));
    $('#projetoDt-modal').modal('hide');
}

$(document).on('keydown', '.pesquisar-projetoDt', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchProjetoDetail(this);
        $('#projetoDt-modal').modal('toggle');
    }
});
//fim input projeto Detail


//inicio input colaborador
function SearchColaborador() {
    var pesquisa = $('#pesquisar-colaborador').val();
    var pesquisa = pesquisa.toUpperCase()

    $.ajax({
        url: '/Searchs/Colaborador',
        type: 'get',
        dataType: 'json',
        data: {
            'pesquisa': pesquisa,
        },
        success: function (data) {
            listarColaborador(data);
        },
    });
}

function listarColaborador(listItens) {
    $('tbody#lista-tabela-filial tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.Id + "' onchange='fnc(this);' data-tipo='colaborador' value='" + item.User + "' data-id='" + item.Id + "'></td>";
        row += "<td>" + item.User + "</td>";
        row += "<td>" + item.Id + "</td>";

        $('tbody#lista-tabela-colaborador').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='colaborador' data-id='" + item.Id + "'>" + row + "<tr>");
    });
}

function preencherColaborador(_radio) {
    $('#pesquisar-colaborador').val(jQuery(_radio).val());
    $('#idColaborador').val(jQuery(_radio).data("id"));
    $('#colaborador-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-colaborador', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchColaborador();
        $('#colaborador-modal').modal('toggle');
    }
});
//fim input colaborador


//inicio input contrato
function SearchContrato() {
    var contrato = $('#pesquisar-contrato').val();
    var contrato = contrato.toUpperCase()

    $.ajax({
        url: '/Searchs/Contrato',
        type: 'get',
        dataType: 'json',
        data: {
            'descricao': contrato,
        },
        success: function (data) {
            listarContrato(data);
        },
    });
}

function listarContrato(listItens) {
    $('tbody#lista-tabela-contrato tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.Id + "Contrato' onchange='preencherContrato(this);' value='" + item.Descricao + "' data-id='" + item.Id + "'></td>";
        row += "<td>" + item.Numero + "</td>";
        row += "<td>" + item.Descricao + "</td>";
        //row += "<td>" + (item.Descricao != "" ? item.Descricao : " '' ") + "</td>";
        //row += "<td>" + item.Status + "</td>";


        $('tbody#lista-tabela-contrato').append("<tr class='tr tr-click' onclick='fnc(this);' data-tipo='contrato' data-id='" + item.Id + "Contrato'>" + row + "<tr>");
    });
}

function preencherContrato(_radio) {
    $('#pesquisar-contrato').val(jQuery(_radio).data("id") + " - " + (jQuery(_radio).val()));
    $('#idContrato').val(jQuery(_radio).data("id"));
    $('#contrato-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-contrato', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchContrato();
        $('#contrato-modal').modal('toggle');
    }
});
//fim input contrato


//inicio input bp
function listarClientes() {
    var CardName = $('#pesquisar-clientes').val();
    var CardName = CardName.toUpperCase()
    //var idCliente = $('#idCliente').val();

    $.ajax({
        url: '/Searchs/SearchCliente',
        type: 'get',
        dataType: 'json',
        data: {
            'CardName': CardName
        },
        success: function (data) {
            listarClientList(data);
        },
    });
}

function listarClientList(listItens) {
    $('tbody#lista-tabela-clientes tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.CardCode + "' onchange='preencherCliente(this)' ' value='" + item.CardName + "' data-cardcode='" + item.CardCode + "'></td>";
        row += "<td>" + item.CardCode + "</td>";
        row += "<td>" + item.CardName + "</td>";

        $('tbody#lista-tabela-clientes').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='cliente' data-id='" + item.CardCode + "'>" + row + "<tr>");
    });
}


function preencherCliente(_radio) {
    $('#pesquisar-clientes').val(jQuery(_radio).val());
    $('#idCliente').val(jQuery(_radio).data("cardcode"));
    $('#cliente-modal').modal('toggle');
}

$(document).on('keydown', '#pesquisar-clientes', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        listarClientes();
        $('#cliente-modal').modal('toggle');
    }
});
//fim input bp


function listaritensfinanceiros() {

    var itemName = $('#pesquisar-item').val();
    var itemName = itemName.toUpperCase()

    $.ajax({
        url: '/Item/SearchFinanceiro',
        type: 'get',
        dataType: 'json',
        data: {
            'itemName': itemName,
        },
        success: function (data) {
            listarItensRadio(data);
        }
    });
}