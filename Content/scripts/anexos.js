var anexos = []

function addAnexoForm(){
    $('#form_item').hide();
    $('#nav-anexos-tab').tab('show');
    $('#form_anexo').show();
}

function cancelarAnexo(){
    $('#form_item').show();
    $('#nav-itens-tab').tab('show');
    $('#form_anexo').hide();
}

function uploadAnexo(){
    var formData = new FormData();

    formData.append('file', $('#input_anexo')[0].files[0]);
    //console.log($('#input_anexo')[0].files[0])

    if ($('#input_anexo').val() == null || $('#input_anexo').val() == "") {
        alert("Nenhum arquivo selecionado")
    } else {
        $.ajax({
            url: '/UploadArquivo/Upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success:function(result){
                addLinhasAnexos(result);
                $('#input_anexo').val('');
            },
            error:function(result){
                console.log('error:', result);
            }
        });
    }
}

function excluirAnexo(index){
    anexos.splice(index,1);
    listarAnexos();
}

function addLinhasAnexos(anexo){
    anexos.push(anexo);
    listarAnexos();
}

function limparAnexos() {
    anexos = [];
    listarAnexos();
}

function listarAnexos(){
    $('tbody#anexos tr').remove();
    anexos.forEach(function(linha, index){
        var row = "";                 
        row += "<td>" + linha.Nome + "</td>";
        row += "<td>" + linha.Extensao + "</td>";
        row += `<td> <button class='btn btn-outline-danger material-icons' onclick='excluirAnexo(${index})'>delete</button></td>`;

        $('tbody#anexos').append("<tr class='tr'>" + row + "<tr>");
    });
}